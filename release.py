#!/usr/bin/env python

from os import path, remove
from sys import exit, stdout
from datetime import datetime
import json
from ruamel.yaml import YAML, RoundTripLoader
from subprocess import check_output, call, run, PIPE
from argparse import ArgumentParser

from pprint import pprint

YAML_PLACEHOLDER = "#__NEW_VERSION__"
yaml_source = "changelog.yaml"
json_changelog = "changelog.json"
g_script_dir = path.dirname(path.realpath(__file__))
g_script_name = path.basename(path.realpath(__file__)).strip(".py")
g_action_list = ['patch', 'minor', 'major']

def _get_versions():
    """
    get current version from package.json
    """
    versions = { 'current': '', 'previous': ''}
    yaml = YAML()

    with open('package.json', 'r') as package:
        data = json.load(package)
        # print(data)
        if "version" in data.keys():
            versions['current'] = data["version"]

    with open(yaml_source, 'r') as source:
        data = yaml.load(source)
        if 'latest_version' in data.keys():
            versions['previous'] = data['latest_version']

    print("Version retrieved:\ncurrent: {}\nprevious: {}"\
          .format(versions['current'], versions['previous']))
    return versions



def _check_repo_clean():
    """ check the state of the git repo """
    # TODO: check that we are on master (or release) branch 
    
    print("script dir is: {}".format(g_script_dir))
    unclean_list = check_output("git status --porcelain".split(" "),
                                cwd=g_script_dir)
    if len(unclean_list):
        print("The repo under {} is not clean:\n{}"
              .format(g_script_dir, unclean_list))
        return False
    return True

def _increment_package_json_version(action):

    if action not in g_action_list:
        print("Unexpected value provided for action: {}, expected value are: {}"
              .format(action, g_action_list))
        return False

    cmd = "npm version {action}".format(action = action)
    try:
        check_output(cmd.split(" "))
    except Exception as e: 
        print("incrementing version encountered an issue: {}".format(e))
        return False
    return True

def _get_user_input(earliest, latest, release_date):
    """
    prompt the user for edition of the commit log history
    returns a string with the changelog info 
    """

    timestamp = release_date.strftime(format="%Y%m%d-%H:%M:%S")
    tmp_file = "/tmp/{}_{}_{}.log".format(timestamp, latest, g_script_name)

    cmd = "git log v{earliest}..v{latest} --pretty=format:\"%s\""\
          .format(earliest=earliest, latest=latest)
    raw_gitlog = check_output(cmd.split(" "),
                              cwd=g_script_dir,
                              encoding='utf-8')
    # print("change logs:\n{}".format(raw_gitlog))

    edition_text = """\
# Modify the lines below for the changelog version
# lines starting with a '#' will be ignored""".split("\n")

    # for all line from the gitlog except first and last
    # return all char execpt first and last
    edition_text.extend([ comment[1:-1] for comment in raw_gitlog.split("\n")[1:-1]])

    with open(tmp_file, 'w') as t:
        for line in edition_text:
            if line[0] != '#':
                line = "- {}".format(line)
            t.write("{}\n".format(line))

    try:
        print("Opening changelog edition with vi, press enter to confirm")
        editor_input = input()
        cmd = "vi {}".format(tmp_file)
        res = call(cmd.split(" "))

        changelog_edited = []
        with open(tmp_file, 'r') as t:
            for line in t.readlines():
                if line[0] != '#':
                    changelog_edited.append(line) 
        print("edition result: {}".format(changelog_edited))
    except Exception as e:
        print("Problem occured will processing file: {}, exception: {}, removing file".format(tmp_file, e))
        remove(tmp_file)
        return False
    return changelog_edited


def _get_yaml_section(version, date, log):
    """ produce the yaml section for the new version """
    
    changelog_date = date.strftime(format="%d/%m/%Y %H:%M:%S")
    if not isinstance(log, list):
        log = [log]
    section_pattern = """\
{placeholder}
- version: {version}
  date: {date} 
  log: |
{log}"""
    indented_logs = "".join(["    {}".format(line) for line in log])
    res = section_pattern.format(placeholder = YAML_PLACEHOLDER,
                                  version = version,
                                  date =changelog_date,
                                  log = indented_logs)
    print("returning: {}".format(res))
    return res

def _update_changelog_yaml(version, date, log):
    """
    updating the changelog.yaml
    """
    yaml = YAML()

    try:
        yaml_read = open(yaml_source, 'r')
        yaml_str = "".join(yaml_read.readlines())
        yaml_read.close()

        # had to do a replace because ruamel has not implemented 
        # orderdict in py3...
        yaml_str = yaml_str.replace(YAML_PLACEHOLDER, 
                         _get_yaml_section(version = version,
                                           date = date,
                                           log = log))
        data = yaml.load(yaml_str)

        if not 'latest_version' in data.keys():
            print("Missing latest_version field in {}".format(yaml_source))
            return False

        data['latest_version'] = version

        # writing the new changelog
        yaml_write = open(yaml_source, 'w')
        yaml.dump(data, yaml_write)
        yaml_write.close()

    except Exception as e:
        print("issue while editing the {}, exceptio: {}".format(yaml_source, e))
        return



def _generate_new_changelog(earliest, latest):
    """ 
    main method for change  log generation 
    """
    release_date = datetime.now()
    
    changelog_edited = _get_user_input(earliest = earliest,
                                        latest = latest,
                                        release_date = release_date)
    _update_changelog_yaml(version = latest, 
                           date = release_date,
                           log = changelog_edited)



def _generate_json():
    with open(yaml_source, 'r') as source:
        yaml = YAML()
        data = yaml.load(source)
        with open(json_changelog, 'w') as dest:
            json.dump(data, dest)


def _prompt_diff():
    gitdiff = check_output("git diff".split(" "),
                                cwd=g_script_dir, encoding="utf-8")
    print("About to commit following diffs:")
    pprint("".join(gitdiff))
    print("Press 'enter' to confirm or any key to quit")

    a = input()
    if a != "":
        return False
    return True


def commit_and_push():
    try:
        print("Commit amending")
        check_output("git add changelog.yaml changelog.json".split(" "), cwd=g_script_dir)
        check_output("git commit --amend --no-edit".split(" "), cwd=g_script_dir)

        print("Pushing")
        check_output("git push --follow-tags".split(" "), cwd=g_script_dir)
    except Exception as e:
        print(f"An issue occured while commiting/push changelog.*: {e}")
        return
    print("done")


def publish_new_docker_image(version):
    print("Running the docker stage")
    cmd = f"bash build.sh {version}".split()
    res = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE)
    if res.returncode != 0:
        print(f"Docker image publishing failed with error:\n{res.stdout}")
        return
    print(f"Docker image publishing successful tag: {version}")


def get_args():
    parser = ArgumentParser(description='release script for yopstack front and backend')
    parser.add_argument('action', choices=g_action_list,
                         help='Specify if the new release is a patch, a minor or major version')
    args = parser.parse_args()
    if not args:
        parser.print_help()
        exit(0)
    return args


if __name__ == "__main__":
    args = get_args()
    # check repo clean
    if not _check_repo_clean():
        print("exiting")
        exit(1)

    # increment version (argument patch, minor, major) needed
    if not _increment_package_json_version(args.action):
        print("exiting")
        exit(1)

    versions = _get_versions()
    if versions['previous'] == "" or \
    versions['current'] == "" or \
    versions['current'] == versions['previous']:
        print("problem identifying version, exiting: {}".format(versions))
        exit(1)


    # propose changelog text based on commit log since previous tag
    # update change_log.yaml
    _generate_new_changelog(earliest=versions['previous'], latest=versions['current'])

    # generate change_log.json
    _generate_json()

    if not _prompt_diff():
        print("exiting")
        exit(1)

    # amend and push (if not already pushed)
    commit_and_push()

    publish_new_docker_image(versions['current'])
