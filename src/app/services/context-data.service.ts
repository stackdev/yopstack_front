import { Injectable }      from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable }      from 'rxjs/Observable';


@Injectable()
export class ContextDataService {

  private default_context : string = "";
  public contextSource = new BehaviorSubject<string>(this.default_context);
  public currentContext = this.contextSource.asObservable();  

  private default_action = "do_nothing";
  public actionSource = new BehaviorSubject<string>(this.default_action);
  public currentAction = this.actionSource.asObservable();

  constructor() { }

  public changeContext(new_context: string){
      this.contextSource.next(new_context);
  }

  public changeAction(new_action: string) {
      this.actionSource.next(new_action);
  } 
}
