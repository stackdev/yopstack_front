export class Stack {
    public is_late : boolean = false;
    public match : boolean = false;
    public last_planned_check: number;
    public checkCount: CheckCount;

  constructor(
    public id: string,
    public name: string,
    public description: string,
    public periodicity: number,
    public last_check: number,
    public last_check_count: number,
    public frequency: number,
    public creation_date: number,
    public type: number,
    public isPrivate: boolean, 
    public status: number, 
    public isShared: boolean,
    public admins: string[],
    public checkers: string[],
    public target_date: number,
    ) { }
}

export class CheckCount {
    constructor(
        public ok: number,
        public planned: number,
        public failed: number,
        public total: number
    ) {}
}
