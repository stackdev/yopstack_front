import { TestBed, inject } from '@angular/core/testing';

import { ContextDataService } from './context-data.service';

describe('ContextDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContextDataService]
    });
  });

  it('should be created', inject([ContextDataService], (service: ContextDataService) => {
    expect(service).toBeTruthy();
  }));
});
