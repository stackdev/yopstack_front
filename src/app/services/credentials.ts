/**
 * Base credentials type
 */
export class Credentials {
  public username: string;
  public password: string;
  constructor(
    username: string,
    password: string) {
    this.username = username;
    this.password = password;
  }
}

/**
 * Credentials for account creation
 */
export class AccountCreationCredentials extends Credentials{
  public email: string;
  constructor(
    username: string, 
    password: string,
    email: string) {
    super(username, password);
    this.email = email;
  }
}
