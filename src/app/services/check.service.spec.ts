import { TestBed, inject } from '@angular/core/testing';
import { CommonService } from '../services/common.service';
import { GlobalService } from '../services/global.service';
import { Http } from "@angular/http";
import { CheckService } from './check.service';

describe('CheckService', () => {
  let mockHttp;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CheckService,
        CommonService,
        GlobalService,
        { provide: Http, useValue: mockHttp }
      ]
    });
  });

  it('should ...', inject([CheckService], (service: CheckService) => {
    expect(service).toBeTruthy();
  }));
});
