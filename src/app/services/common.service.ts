import { Injectable }    from '@angular/core';
import { StackType } from './global.service';


@Injectable()
export class CommonService {

  public base                 = "";
  public base_uri             = this.base + "/aggregators";
  public base_uri_users       = this.base_uri + "/users";
  public base_uri_stacks      = this.base_uri_users + "/{uid}/stacks";
  public base_uri_checks      = this.base_uri + "/checks/";
  public base_uri_comments    = this.base_uri_stacks +"/{sid}/checks/{cid}/comments/" 
  public base_uri_comments_v2 = this.base_uri_stacks +"/{sid}/comments/" 
  public base_uri_preferences = this.base_uri_users + "/{uid}/preferences/";
  public logout_uri           = this.base + "/logout"

  public base_info_uri        = this.base + "/actuator";
  public health_uri           = this.base_info_uri + "/health";
  public info_uri             = this.base_info_uri + "/info";

  constructor() {
  }


  public setStackTypeClass( type: StackType, element: any, sub_element_str: string ) {
    console.log("type: ", type, "ENUM: ", StackType.TIME, ", ", StackType.NOTES, ", ", StackType.SHARED);
    //console.log("value observed: ", stack);
    var stack_class = "time";
    if (type === StackType.TIME) {
      stack_class = "time";
    }
    if (type === StackType.NOTES) {
      stack_class = "notes";
    }
    if (type === StackType.SHARED) {
      stack_class = "shared";
    }
    console.log("element: ", element);
    console.log("sub_element_str: ", sub_element_str);

    element.classList.add(stack_class);

  }

 public addHours(my_date: Date, h: number) {    
   my_date.setTime(my_date.getTime() + (h*60*60*1000)); 
   return my_date;   
 }

 public groupBy(anArray: any[], key: any) {
  return anArray.reduce(function(rv, x) {
    let group = 'not_found';
    if ( key in x ) {
      group = x[key];
    }
      (rv[group] = rv[group] || []).push(x);
    return rv;
  }, {});
};

/**
 * reverting an object, values to keys. 
 * Non unique values leads to last value to be kept
 * @param {[type]} obj [description]
 */
public reverseObject(obj) {
   const newObj = {};
   Object.keys(obj).forEach(key => {
         newObj[obj[key]] = key;
   });
   return newObj;
};

} // end 

