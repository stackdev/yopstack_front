import { Injectable }                                           from '@angular/core';
import {BaseRequestOptions, RequestOptions, RequestOptionsArgs} from '@angular/http';

import { GlobalService } from './global.service';

@Injectable()
/**
 * Adding authorization header to all outgoing requests
 */
export class DefaultRequestOptions extends BaseRequestOptions {
    private globalService : GlobalService;

    constructor(globalService: GlobalService){
      super();
      this.globalService = globalService;
    }

    merge(options?: RequestOptionsArgs): RequestOptions {
      var newOptions = super.merge(options);
      if (this.globalService.authorization && this.globalService.authorization != "") {
        newOptions.headers.set('Authorization', this.globalService.authorization);
      }
      // always add X-Requested-With: XMLHttpRequest 
      newOptions.headers.set('X-Requested-With', 'XMLHttpRequest');

      console.log("in request option: ", newOptions);

      return newOptions;
    }
}