import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { URLSearchParams, QueryEncoder } from "@angular/http";
import { Observable } from "rxjs/Observable";

import { CommonService } from "./common.service";

import { environment } from '../../environments/environment';

@Injectable()
export class BackendInfoService {
  public commonService: CommonService;
  private infoUrl: string;

  constructor(private http: Http, commonService: CommonService) {
    this.commonService = commonService;
    this.infoUrl = environment.baseUrl + this.commonService.info_uri;
  }

  public getBackendInfo(): Observable<any> {
    let headers = new Headers({ "Content-Type": "application/json" });
    let params = new URLSearchParams();

    /*params.set("before_date", before_date);
    params.set("after_date", after_date);*/
    let options = new RequestOptions({
      params: params,
      headers: headers,
      withCredentials: true
    });
    console.log("getting backend information with url: ", this.infoUrl);
    return this.http
      .get(this.infoUrl, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) : Observable<BackendVersion> {
    // console.log("Extracting body from: ", res);
    let body = res.json();
    // console.log("extractData: body:", body);
    return body;
  }

  private handleError (error: Response | any) {
    console.log("in error");
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
    }
}

export class BackendVersion {
  constructor(
    public version: string,
    public date: number,
    public changeLog: [any]
  ) {}
}
