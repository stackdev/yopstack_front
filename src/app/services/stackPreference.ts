//import { Preference } from '../services/preference';


export class StackPreference {

  constructor(
    public stackId: string,
    public showMissedPeriods: boolean,
    public showPeriodInCheck: boolean ,
    public enableMarkdownInCheck: boolean,
    public enableMarkdownInComment: boolean,
    ) { }


    // public static fromPreference(stackId: string, preference: Preference) {
    //     return new StackPreference(stackId, !preference.disableMissedPeriod.includes(stackId),
    //         preference.displayCheckPeriod.includes(stackId),
    //         preference.disableMarkdownInCheck.includes(stackId),
    //         preference.disableMarkdownInComment.includes(stackId));
    // }
}
