
export class Preference {
  public language: number;
  public timezone: number;
  public theme: number;
  
  constructor(
    public id:           string,
    public user:         string,
    public stacksorting: number, 
    public risingsort:   boolean,

    public displayCheckPeriod: string[],
    public disableMissedPeriod: string[],
    public disableMarkdownInCheck: string[],
    public disableMarkdownInComment: string[],
    ) { }
}
