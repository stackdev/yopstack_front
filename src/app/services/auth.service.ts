import { Injectable }   from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { RouterModule, Routes, Router} from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

//import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CookieService }                  from 'ngx-cookie';

import { GlobalService } from './global.service';
import { CommonService } from './common.service';
import { UserService } from './user.service';
import { User } from './user';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  private user : string;
  private _lastuser_cookies : string;
  private cookies: any;


  constructor(private commonService: CommonService, private router: Router, private global: GlobalService, 
              private userService: UserService, private cookieService: CookieService, private http: Http) {
      this.user = '';
      this._lastuser_cookies = 'last_user';
  }

  public loggedIn(username: string) : Observable<User> {

    let authenticatedUserName : string;
    if (this.global.authenticatedUser) {
      authenticatedUserName = this.global.authenticatedUser.name;
    } 
    console.log("authenticatedUser: " + authenticatedUserName +
     ", Cookies value: " + this.cookieService.get(this._lastuser_cookies));

    if(username && username != "") {
      console.log("User name provided, verifying its auth status");
      this.user = username;
      console.log(this.user);
    } else {
        // Do we have a user name ?
        console.log("No user provided, check from global service");
        this.user = authenticatedUserName;
        if(!this.user) {
          this.user = this.cookieService.get(this._lastuser_cookies);
          if (!this.user) {
            console.log("No user available... login required");
            this.router.navigate(['login']);
            throw("not logged in"); 
          }
       }
    }
      // Check if user loggedin
      console.log("Calling user resource with user name: " + this.user);
      let userObservable: Observable<User> = this.userService.getAUserByName(this.user)


      userObservable.subscribe(
        value => {
          console.log("success: ", value);
          // success, store user in authenticatedUser and last_user in cookies
          this.global.authenticatedUser = value;
          this.global.authenticated      = true;
          this.cookieService.put(this._lastuser_cookies, this.user);

          console.log("resolving the login GET");
          console.log("authenticatedUser: " + this.global.authenticatedUser.name + ", Cookies value: " + this.cookieService.get(this._lastuser_cookies));
          },
        e     => { 
          console.error(e);
          this.authCleanup();
          this.router.navigate(['login']);
        });
      return userObservable;
    }


    public logout() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({  headers: headers , withCredentials: true });

        var url = environment.baseUrl + this.commonService.logout_uri; 
        return this.http.post(url , {}, options);
    }

    public authCleanup() {
        this.global.authenticatedUser = undefined;
        this.global.authenticated = false;
        this.cookieService.remove(this._lastuser_cookies);       
    }
}
