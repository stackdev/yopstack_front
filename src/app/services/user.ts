import { Avatar } from "./avatar";

export class User {
  constructor(
    public id: string,
    public name: string,
    public list_friend: string[], 
    public list_stack:  string[],
    public enabled: boolean,
    public roles: string[],
    public setpw: string,
    public oldpw: string,
    public avatar: Avatar
    ) { }
}
