import { TestBed, inject } from '@angular/core/testing';
import { Http } from "@angular/http";

import { UserService } from './user.service';
import { GlobalService } from './global.service';
import { CommonService } from './common.service';

describe('UserService', () => {

  let mockHttp;


  beforeEach(() => {
    mockHttp = jasmine.createSpyObj("Http", ["get", "post", "head"])
    TestBed.configureTestingModule({
      providers: [
        UserService,
        GlobalService,
        CommonService, 
        { provide: Http, useValue: mockHttp },

      ]
    });
  });

  it('should ...', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
