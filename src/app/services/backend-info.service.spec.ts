import { TestBed, inject } from '@angular/core/testing';
import { Http } from "@angular/http";

import { CommonService } from '../services/common.service';
import { GlobalService } from '../services/global.service';

import { BackendInfoService } from './backend-info.service';

describe('BackendInfoService', () => {

  let mockHttp;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BackendInfoService,
        CommonService,
        GlobalService,
        { provide: Http, useValue: mockHttp }
      ]
    });
  });

  it('should be created', inject([BackendInfoService], (service: BackendInfoService) => {
    expect(service).toBeTruthy();
  }));
});
