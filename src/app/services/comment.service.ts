import { Injectable }                    from '@angular/core';
import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

import { environment }   from '../../environments/environment';
import { CommonService } from './common.service';


import { Observable} from 'rxjs/Observable';
import { of } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { YopComment } from './comment';

@Injectable()
export class CommentService {

  private commonService: CommonService;
  private commentsUrl: string;
  private commentsUrlv2: string;

  constructor(private http: Http, commonService: CommonService) { 
    this.commonService = commonService;
    this.commentsUrl = environment.baseUrl + this.commonService.base_uri_comments; 
    this.commentsUrlv2 = environment.baseUrl + this.commonService.base_uri_comments_v2; 
  }


  public getCommentsBetweenDates(uid: string, sid: string, cid: string, before_date: string, after_date: string): Observable<YopComment[]> {
    console.log("getting comment with: uid: ", uid, " sid: ", sid, " cid: ", cid);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let params = new URLSearchParams();
    params.set("before_date", before_date);
    params.set("after_date", after_date);
    let options = new RequestOptions({ params: params, headers: headers , withCredentials: true });
    let url = this.commentsUrl.replace('{uid}', uid)
                              .replace('{sid}', sid)
                              .replace('{cid}', cid);
    console.log("getting comment with url: ", url)
    return this.http.get(url, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public getCommentsForChecks(uid: string, sid: string, checkids: string[]): Observable<YopComment[]> {
    console.log("getting comment with: uid: ", uid, " sid: ", sid, " checkis: ", checkids);
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let params = new URLSearchParams();
    params.set("checkids", checkids.join(','));
    let options = new RequestOptions({ params: params, headers: headers , withCredentials: true });
    let url = this.commentsUrlv2.replace('{uid}', uid)
                              .replace('{sid}', sid);
    console.log("getting comment with url: ", url)
    return this.http.get(url, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public saveAComment(stackUserid: string, stackid: string, checkid: string, comment_to_save: YopComment): Observable<YopComment> {
    let post_url = this.commentsUrl.replace('{uid}', stackUserid)
                                   .replace('{sid}', stackid)
                                   .replace('{cid}', checkid);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json_comment = JSON.stringify(comment_to_save);

    return this.http.post(post_url, json_comment, options)
    .map(this.extractData)
    .catch(this.handleError);
  }


  /**
   * [updateAComment description]
   * @param  {YopComment}               modified_comment [description]
   * @return {Observable<Comment[]>}                  [description]
   */
  public updateAComment(modified_comment: YopComment): Observable<YopComment[]> {
    let put_url = this.commentsUrl.replace('{uid}', modified_comment.stack_userid)
                                  .replace('{sid}', modified_comment.stackid)
                                  .replace('{cid}', modified_comment.checkid) 
                                  + modified_comment.id.toString();
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json_comment = JSON.stringify(modified_comment);

    return this.http.put(put_url, json_comment, options)
    .map(this.extractData)
    .catch(this.handleError);
  }



  public getComments(uid: string, sid: string, cid: string): Observable<YopComment[]> {
    console.log("getting comment with: uid: ", uid, " sid: ", sid, " cid: ", cid);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers , withCredentials: true });
    let url = this.commentsUrl.replace('{uid}', uid)
                              .replace('{sid}', sid)
                              .replace('{cid}', cid);
    console.log("getting comment with url: ", url)
    return this.http.get(url, options)
    .map(this.extractData)
    .catch(this.ignoreError);
  }

//TO BE TESTED
  public createComment(name: string): Observable<YopComment> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers , withCredentials: true });

    return this.http.post(this.commentsUrl, { name }, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

/**/
  private extractData(res: Response) {
    // console.log("Extracting body from: ", res);
    let body = res.json();
    // console.log("extractData: body:", body);
    return body || { };
  }


  private extractDataArrayFirst(res: Response) {
    // console.log("extracting: ", res);
    let body = res.json();
    return body[0] || { };
  }


  private handleError (error: Response | any) {
    console.log("in error");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
    }

  private ignoreError (error: Response | any) {
      console.log("in ignore error");
      return of(null);
    }
  }

