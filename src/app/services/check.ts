export class Check {
  //additional field used for later enrichnment
  public period: number = 0;
  public isFrontOnly: boolean;
  public formattedComment: string;

  constructor(
    public id:      string,
    public date:    number,
    public stack:   string,
    public comment: string,
    public status:  string,
    public checker: string
    ) { }
}
