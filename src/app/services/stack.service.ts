import { Injectable }                    from '@angular/core';
import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

import { environment }   from '../../environments/environment';
import { CommonService } from './common.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Stack } from './stack';

@Injectable()
export class StackService {

  private commonService: CommonService;

  private stacksUrl: string;

  constructor(private http: Http, commonService: CommonService) { 
    this.commonService = commonService;
    this.stacksUrl = environment.baseUrl + this.commonService.base_uri_stacks + "/"; 
  }

  public getŚtacks(): Observable<Stack[]> {
    return this.http.get(this.stacksUrl)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public getAStackById(uid: string, sid: string): Observable<Stack> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers , withCredentials: true });
    let url = this.stacksUrl.replace('{uid}', uid) + sid;
    console.log("getting: ", url);
    return this.http.get( url , options)
    .map(this.extractData)
    .catch(this.handleError);
  }


  public getAllStacksByUserid(uid: string): Observable<Stack[]> {
    console.log("Searching stacks from user: ", uid);
    let url = this.stacksUrl.replace('{uid}', uid);
    console.log("with url: ", url);


    return this.http.get( url , { withCredentials: true })
    .map(this.extractData)
    .catch(this.handleError);
  }

  public updateAStack(userid: string, modified_stack: Stack): Observable<Stack> {
    let put_url = this.stacksUrl.replace('{uid}', userid)                                   
                                  + modified_stack.id.toString();
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json_stack = JSON.stringify(modified_stack);

    return this.http.put(put_url, json_stack, options)
    .map(this.extractData)
    .catch(this.handleError);
  }


  public deleteAStack(userid: string, stackid: string): Observable<Stack> {
    let delete_url = this.stacksUrl.replace('{uid}', userid)                                   
                                  + stackid.toString();
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });

    return this.http.delete(delete_url, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public createStack(user_id: string, stack_to_be_created: Stack): Observable<Stack> {
    let post_url = this.stacksUrl.replace('{uid}', user_id);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json_stack = JSON.stringify(stack_to_be_created);

    return this.http.post(post_url, json_stack, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  

/**/
  private extractData(res: Response) {
    // console.log("Extracting body from: ", res);
    let body = res.json();
    // console.log("extractData: body:", body);
    return body || { };
  }


  private extractDataArrayFirst(res: Response) {
    // console.log("extracting: ", res);
    let body = res.json();
    return body[0] || { };
  }

  private handleError (error: Response | any) {
    console.log("in error");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
    }
  }

