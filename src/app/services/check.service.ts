import { Injectable }                    from '@angular/core';
import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

import { environment } from '../../environments/environment';
import { CommonService } from './common.service';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Check } from './check';

@Injectable()
export class CheckService {

  private checksUrl: string;
  private commonService: CommonService;

  constructor(private http: Http, commonService: CommonService) { 
  this.commonService = commonService;
  this.checksUrl = environment.baseUrl + this.commonService.base_uri_checks;

  }

  /**
   * get checks, between 2 dates
   * @param  {string}              stackId     [description]
   * @param  {string}              before_date [description]
   * @param  {string}              after_date  [description]
   * @return {Observable<Check[]>}             [description]
   */
  public getChecksBetweenDates(stackId: string, before_date: string, after_date: string): Observable<Check[]> {
    let params = new URLSearchParams();
    params.set('stack',       stackId);
    params.set('before_date', before_date);
    params.set('after_date',  after_date);

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ params: params, headers: headers , withCredentials: true });
    
    return this.http.get(this.checksUrl, options)
    // .map(this.extractData)
    .map(this.extractChecks)
    .catch(this.handleError);
  }

  /**
   * Update a check
   * @param  {Check}               modified_check [description]
   * @return {Observable<Check[]>}                [description]
   */
  public updateACheck(modified_check: Check): Observable<Check> {
    let put_url = this.checksUrl + modified_check.id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    
    return this.http.put(put_url, JSON.stringify(modified_check), options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public createACheck(check_to_create: Check): Observable<Check> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers , withCredentials: true });

    return this.http.post(this.checksUrl, JSON.stringify(check_to_create), options)
    .map(this.extractData)
    .catch(this.handleError);
  }


  public deleteACheck(check_to_delete: Check): Observable<Check> {
    let delete_url = this.checksUrl + check_to_delete.id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    
    return this.http.delete(delete_url, options)
    .map(this.extractData)
    .catch(this.handleError);
  }



// tested ? 
  public getACheckById(id: string): Observable<Check> {
    let get_url = this.checksUrl + id;
    console.log("getting: ", get_url);
    return this.http.get( get_url )
    .map(this.extractData)
    .catch(this.handleError);
  }

  private extractChecks(res: Response) : Check[]{
    let body = res.json();
    console.log("extractData: body:", body);
    let result_checks = [];
    body.forEach(check => {result_checks.push(new Check(check.id, check.date, check.stack, check.comment, check.status, check.checker))})
    return result_checks;
  }


  private extractData(res: Response) {
    let body = res.json();
    console.log("extractData: body:", body);
    return body || { };
  }


  private extractDataArrayFirst(res: Response) {
    // console.log("extracting: ", res);
    let body = res.json();
    return body[0] || { };
  }

  private handleError (error: Response | any) {
    console.log("in error");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

