import { Injectable }  from '@angular/core';
import { User } from './user';
import { Preference } from './preference';

export enum StackType  { 
  TIME   = 0, 
  NOTES  = 1, 
  SHARED = 2,
  KANBAN = 3
}

export enum StackSorting  { 
  CREATION   = 0, 
  TYPE       = 1, 
  LAST_CHECK = 2
}

export enum StackStatus  { 
  RUNNING  = 0, 
  PAUSED   = 1, 
  ARCHIVED = 2
}

export enum CheckStatus  { 
  PLANNED = 0, 
  OK      = 1,
  FAILED  = 2,
  SKIPPED = 3,
  DOING   = 4
}

export enum EditionMode  { 
  CREATION = 0, 
  UPDATE   = 1
}


@Injectable()
export class GlobalService {

  // being replaced by authenticatedUser
  public authenticatedUser: User;

  public authenticated : boolean         = false;
  public authorization : string;

  public identified_user : User;

  public userPreference: Preference;

  public context_name: string = "";

  private current_context = "";


  constructor() {}

  /**
   * Provide the current context used for the contextual 
   * footer menu
   * @return {string} the current context
   */
  public getCurrentContext() : string {
    return this.current_context;
  }

  public setContextStackEdition() {
    this.current_context="stack_context";
  }

  public keys(_enum) : Array<string> {
    let keys = Object.keys(_enum);
    return keys.slice(keys.length / 2);
  }
}


