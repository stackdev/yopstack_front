import { TestBed, inject, async } from '@angular/core/testing';

import { StackUiLogicService } from './stack-ui-logic.service';
import { UserService } from './user.service';
import { Check } from './check';

fdescribe('StackUiLogicService', () => {
  let mockUserService;

  beforeEach(() => {
    mockUserService = jasmine.createSpyObj("UserService", ["getAUserById"]);

    TestBed.configureTestingModule({
      providers: [
        StackUiLogicService,
        { provide: UserService, useValue: mockUserService }
      ]
    });
  });

  it('Should create ', inject([StackUiLogicService], (service: StackUiLogicService) => {
    expect(service).toBeTruthy();
  }));

  const oneDayMs = 24 * 60 * 60 * 1000;
  describe('fillGapsInChecks method', () => {

    beforeEach(()=>{
        this.stack_creation_date = new Date(new Date().getTime() - (12 * oneDayMs));
        this.referenceDate = (new Date().getTime()) - (3 * oneDayMs);;
        this.dateFourDaysBefore = this.referenceDate - (4 * oneDayMs) ;
        this.checks = [new Check("1", this.referenceDate, "stack", "check 1", "OK"),
                      new Check("2", this.dateFourDaysBefore , "stack", "check 2", "OK")];
        this.stack_periodicity = 2;
        this.stack_frequency = 1;
    });

    it('should return a list of check filled with missed checks',
      inject([StackUiLogicService], (service: StackUiLogicService) => {

        let result = service.fillGapsInChecks(this.checks, this.stack_creation_date, this.stack_periodicity, this.stack_frequency);
console.log("TMP::",result);
        expect(result).toBeTruthy();
        expect(result.length).toEqual(this.checks.length + 2);
        expect(result[2].status).toEqual("FAILED");
      }));

    it('should return a list of check filled with future checks',
      inject([StackUiLogicService], (service: StackUiLogicService) => {

        let result = service.fillGapsInChecks(this.checks, this.stack_creation_date, this.stack_periodicity, this.stack_frequency);

        expect(result).toBeTruthy();
        expect(result.length).toEqual(this.checks.length + 2);
        expect(result[0].status).toEqual("PLANNED");
      }));

    it('should return a list of check filled with missed checks count compare to current time',
      inject([StackUiLogicService], (service: StackUiLogicService) => {

        this.checks = [new Check("2", this.dateFourDaysBefore , "stack", "check 2", "OK")];

        let result = service.fillGapsInChecks(this.checks, this.stack_creation_date, this.stack_periodicity, this.stack_frequency);

        expect(result).toBeTruthy();
        expect(result.length).toEqual(this.checks.length + 2);
        expect(result[1].status).toEqual("FAILED");
      }));
  });

  describe('periodToDate method', () => {
    it('should return a date in the middle of the given period 1',
    inject([StackUiLogicService], (service: StackUiLogicService) => {
      expect(service.periodToDate(1, new Date('2019-01-01T10:00:00Z'), 2)).toEqual(new Date('2019-01-04T10:00:00Z'));
    }))

    it('should return a date in the middle of the given period for period 40',
    inject([StackUiLogicService], (service: StackUiLogicService) => {
      expect(service.periodToDate(40, new Date('2019-01-01T10:00:00Z'), 2)).toEqual(new Date('2019-03-23T10:00:00Z'));
    }))
  });


  it('Calculate period returns the correct period',
    inject([StackUiLogicService], (service: StackUiLogicService) => {
      let threeDaysMs = 3 * oneDayMs;
      expect(service.calculatePeriod(0, 2, threeDaysMs)).toEqual(1);

  }));

  it('Calculate period returns negative value if the date is before the reference date',
    inject([StackUiLogicService], (service: StackUiLogicService) => {
      let referenceDate: number = (new Date()).getTime();
      let threeDaysBeforeReferenceDate: number = (new Date(referenceDate)).setDate((new Date(referenceDate)).getDate() - 3);

      let result = service.calculatePeriod(referenceDate, 2, threeDaysBeforeReferenceDate);

      expect(result).toEqual(-2);

  }));



});
