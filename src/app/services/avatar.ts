
export class Avatar {
  constructor(public version: string,
              public type:    number, 
              public data:    string) { }
}
