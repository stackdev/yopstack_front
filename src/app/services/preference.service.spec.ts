import { TestBed, inject } from '@angular/core/testing';
import { Http } from "@angular/http";

import { PreferenceService } from './preference.service';
import { CommonService } from './common.service';

describe('PreferenceService', () => {
  let mockHttp;

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj("Http", ["get", "post", "put"]);

    TestBed.configureTestingModule({
      providers: [
        PreferenceService,
        { provide: Http, useValue: mockHttp },
        CommonService
      ]
    });
  });

  it('should ...', inject([PreferenceService], (service: PreferenceService) => {
    expect(service).toBeTruthy();
  }));
});
