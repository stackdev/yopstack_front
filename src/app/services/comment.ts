export class YopComment {
  constructor(
    public id:                string,
    public stack_userid:      string,
    public stackid:           string,
    public checkid:           string,
    public comment_userid:    string, 
    public text:              string,
    public text_formatted:    string,
    public timestamp:         number,
    public update_timestamp:  number
    ) { }
}

