import { TestBed, inject } from '@angular/core/testing';

import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { Http }  from '@angular/http';

import { AuthService } from './auth.service';
import { CommonService } from './common.service';
import { GlobalService } from './global.service';
import { UserService } from './user.service';


describe('AuthService', () => {
  let mockRouter;
  let mockGlobalService;
  let mockUserService;
  let mockCookieService;
  let mockHttp;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
      AuthService,
      CommonService,
      { provide: Router, useValue: mockRouter },
      GlobalService,
      { provide: UserService, useValue: mockUserService },
      { provide: CookieService, useValue: mockCookieService },
      { provide: Http, useValue: mockHttp },

      ]
    });
  });

  it('should ...', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));
});
