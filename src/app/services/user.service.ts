import { Injectable }                    from '@angular/core';
import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { GlobalService } from './global.service';
import { CommonService } from './common.service';
import { User } from './user';

@Injectable()
export class UserService {

  private globalService: GlobalService;
  private commonService: CommonService;

  private usersUrl: string;
  private headers: Headers;
  private options: RequestOptions;

  constructor(private http: Http, globalService: GlobalService, commonService: CommonService) { 
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers , withCredentials: true });
    this.globalService = globalService;
    this.commonService = commonService;

    this.usersUrl = environment.baseUrl + this.commonService.base_uri_users;  // URL to web API
  }

  public getUsers(): Observable<User[]> {
    return this.http.get(this.usersUrl)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public getAUserById(id: string): Observable<User> {
    var get_url = this.usersUrl +"/"+ id;
    console.log("getting: ", get_url);
    return this.http.get( get_url , this.options )
    .map(this.extract)
    .catch(this.handleError);
  }

  public getAUserByName(name: string): Observable<User> {
    let headers: Headers = new Headers();
    let params = new URLSearchParams();
    params.set('name', name);

    return this.http.get( this.usersUrl, { params: params, headers: headers,  withCredentials: true} )
    .map(this.extractDataArrayFirst)
    .catch(this.handleError);
  }


  public checkUserNameAvailibility(name: string): Observable<any> {
    return this.http.head(this.usersUrl + "/" + name)
          .map(this.extractStatusOK)
          .catch((err, caught) => Observable.create(observer => { observer.next(true);observer.complete();}));
  }

  /**
   * [getListUserByNameLike description] grabbing user list matching a pattern provided as url param
   * @param  {string}           name_like pattern
   * @return {Observable<User>}           list of user as an observable
   */
  public getListUserByNameLike(name_like: string): Observable<User[]> {
    let headers: Headers = new Headers();
    let params = new URLSearchParams();
    params.set('name', name_like);
    params.set('exact', "false");

    return this.http.get( this.usersUrl, { params: params, headers: headers,  withCredentials: true} )
    .map(this.extract)
    .catch(this.handleError);
  }

  public updateAUser(modified_user: User): Observable<User> {
    let put_url = this.usersUrl + "/" + modified_user.id.toString();
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json_user = JSON.stringify(modified_user);

    return this.http.put(put_url, json_user, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

/**
 * Method used for login purpose, should not be cached !
 * If no password provided, no auth header is set
 * @param  {string}           name     [description]
 * @param  {string}           password [description]
 * @return {Observable<User>}          [description]
 */
  public doLogin(name: string, password: string): Observable<User> {
    console.log("in doLogin user:", name);
    let headers: Headers = new Headers();
    console.log("auth header");
    let authorization_value = "Basic " + btoa(name + ":" + password);
    this.globalService.authorization = authorization_value;
    let params = new URLSearchParams();
    params.set('name', name);

    return this.http.get( this.usersUrl, { search: params, headers: headers} )
    .map(this.extractDataArrayFirst)
    .catch(this.handleError);
  }


  public create(newUser: User): Observable<User> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers});
    let jsonUser = JSON.stringify(newUser);

    return this.http.post(this.usersUrl, jsonUser, options)
          .map(this.extractData)
          .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log("extractData: body:", body);
    return body.data || { };
  }

  private extractStatusOK(res: Response) {
    console.log("res: ", res);
    if (res.status === 200 && res.ok) {
      return true;
    }
    return false;
  }

  private extractStatusFailedExpected(res: Response) : boolean {
    console.log("Failed expected: res: ", res);
    if (res.status !== 200) {
      return true;
    }
    return false;
  }

  private extractStatusFailed(res: Response) {
    return Observable.throw("");
  }



  private extract(res: Response) {
    let body = res.json();
    console.log("extract: body:", body);
    return body || { };
  }


  private extractDataArrayFirst(res: Response) {
    //console.log("extracting: ", res);
    let body = res.json();
    return body[0] || { };
  }

  private handleError (error: Response | any) {
    console.log("in error: ", error);
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      //console.error(errMsg);
      return Observable.throw(errMsg);
    }
  }
