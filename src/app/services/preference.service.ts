import { Injectable }                    from '@angular/core';
import { Http, Response }                from '@angular/http';
import { Headers, RequestOptions }       from '@angular/http';
import { URLSearchParams, QueryEncoder } from '@angular/http';

import { environment }   from '../../environments/environment';
import { CommonService } from './common.service';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Preference } from './preference';

@Injectable()
export class PreferenceService {

  private commonService: CommonService;
  private preferencesUrl: string;

  constructor(private http: Http, commonService: CommonService) { 
    this.commonService = commonService;
    this.preferencesUrl = environment.baseUrl + this.commonService.base_uri_preferences; 
  }


  public getPreference(uid: string, pid: string){
    console.log("Getting preference with: uid: ", uid, " pid: ", pid);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let params = new URLSearchParams();
    let options = new RequestOptions({ headers: headers , withCredentials: true });
    let url = this.preferencesUrl.replace('{uid}', uid)
    .replace('{pid}', pid);
    console.log("getting preference with url: ", url)
    return this.http.get(url, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public saveAPreference(preference_to_save: Preference): Observable<Preference> {
    let url = this.preferencesUrl.replace('{uid}', preference_to_save.user);
    /*let post_url = this.commentsUrl.replace('{uid}', stackUserid)
                                   .replace('{sid}', stackid)
                                   .replace('{cid}', checkid);*/
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json = JSON.stringify(preference_to_save);

    return this.http.post(url, json, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public updateAPreference(preference_to_update: Preference): Observable<Preference> {
    let url = this.preferencesUrl.replace('{uid}', preference_to_update.user );
    url = url + preference_to_update.id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({  headers: headers , withCredentials: true });
    let json = JSON.stringify(preference_to_update);

    return this.http.put(url, json, options)
    .map(this.extractData)
    .catch(this.handleError);
  }


private extractData(res: Response) {
  //console.log("Extracting body from: ", res);
  let body = res.json();
  //console.log("extractData: body:", body);
  return body || { };
}


private extractDataArrayFirst(res: Response) {
  //console.log("extracting: ", res);
  let body = res.json();
  return body[0] || { };
}


private handleError (error: Response | any) {
  console.log("in error");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // console.log("is response", error);
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
    }

    private ignoreError (error: Response | any) {
      console.log("in ignore error");
      return Observable.of(null);
    }
  }

