import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { UserService } from './user.service';
import { CheckStatus } from './global.service';
import { User }        from './user';
import { Check }       from './check';
import { Stack }       from './stack';
import { YopComment }  from './comment';

import { StackType }   from '../services/global.service';
import * as marked     from 'marked';


const oneDayMs: number = 1000 * 60 * 60 * 24;

@Injectable()
export class StackUiLogicService {

  userService: UserService;

  constructor(userService: UserService) {
    this.userService = userService; 
  }


     /**
     * Compute next check deadline
     */
     public computeNextCheckDate(frequency: number, periodicity: number, last_check: Date, creation_date: Date) {
      var self = this;
      var current_period_checked = false;

      if (!(frequency && periodicity && last_check && creation_date)) {
        console.log("Some parameters are missing in the given stack !");
        console.log(frequency  + " && " + periodicity + " && " +  last_check + " && " +  creation_date);
        return new StackTimeDetails(current_period_checked, null, null);
        } else {

        // get current date period
        var now = new Date();
        var current_period = self.getPeriod(now, creation_date, periodicity);

        // get last check period
        var last_check_period = self.getPeriod(last_check, creation_date, periodicity);

        //console.log("Current period stack period is : " + current_period);
        var computed_period = current_period;
        // if last check period == current period, return next period + already checked bool
        // else return current period + not yet checked
        if (current_period == last_check_period ) {
          computed_period = current_period + 1;
          current_period_checked = true;
          //console.log("Current period and last_check period match: (last check date: " + new Date(last_check) + ", computing next period: " + computed_period);

        }

        var computed_period_start    = creation_date.getTime() +  computed_period       * periodicity * oneDayMs;
        var computed_period_end      = creation_date.getTime() + (computed_period + 1 ) * periodicity * oneDayMs;
        var computed_period_start_date = new Date(1970,0,1).setSeconds(computed_period_start / 1000);
        var computed_period_end_date   = new Date(1970,0,1).setSeconds(computed_period_end   / 1000);
        //console.log("Returning: period start_date: " + new Date(computed_period_start_date) +", end_date: "+ new Date(computed_period_end_date), "current_period_checked: ", current_period_checked);

        return new StackTimeDetails(current_period_checked, new Date(computed_period_start_date), new Date(computed_period_end_date));
      }
    }


    /**
    * Time before date
    */
    public timeBeforeDate(date1: Date, date2: Date) {
      console.log("in timeBeforeDate: " + date1 + " .. " + date2);
      
      var date1_s = date1.getTime();
      var date2_s = date2.getTime();

      if (date1_s > date1_s) { return {};}

      var diff = new Date(date2_s - date1_s);

      var years = diff.getUTCFullYear() - 1970; // Gives difference as year
      var months = diff.getUTCMonth(); // Gives month count of difference
      var days = diff.getUTCDate()-1;  // Gives day count of difference
      var hours = diff.getUTCHours();
      var min = diff.getUTCMinutes();
      var sec = diff.getUTCSeconds();
      console.log("Diff between : " + date1 + " and : " + date2 + " is : ");
      console.log("Y: " + years + "M: " + months + "D: " + days);
      console.log("H: " + hours + "m: " + min + "s: " + sec);
    }


    /**
    * rework of username replacement
    */
    public setUserNameInComments(comments: YopComment[][]) {
      const self = this;      
      let uniq_uid_list = [];
      console.log("incomming comments: ", comments);

      for(var key in comments) {
        console.log(comments[key]);
        comments[key].map(function(current_comment) {
          if ('username' in current_comment ) {
              //console.log("No need to set the username in the comment");
          } else {
              //skip userid not defined...
              if (current_comment['comment_userid']) {
                // get list of unique uid used in the checks comments
                if ( uniq_uid_list.indexOf(current_comment['comment_userid']) === -1 ) {
                  uniq_uid_list.push(current_comment['comment_userid']);
                }
              }
            }
            });
      }


      console.log("list of UID to be retrived: " + uniq_uid_list);

      // for each uid we do a call to the UserResource
      // get the list of uid
      var resources = [];
      uniq_uid_list.map(function(current_uid) {
        console.log("Calling UserResource2 with : " + current_uid );
        resources.push(self.userService.getAUserById(current_uid));
        });


      //FIXME: q.all should be taking an array of promise.then().catch()... to avoid having one fail user retrieval to cause pb for the rest of processing
      // if user not found don't add it to the the array of known user...
      Observable.forkJoin(resources)
      .subscribe(result_uid => {
        console.log("in q.all().then(): ");
        console.log(result_uid);
        let uid_username_cache: Map<string, string> = new Map<string, string>();
        result_uid.map(function(current: User) {
            //console.log("Current state : " + current.state);
            //if ( current.state === "fulfilled") {
              console.log(current.name + ", " + current.id);
              uid_username_cache[current.id] = current.name;
            //}
            });

        // once all the promise are resolved we add a property in the checks object list
        self.refreshCommentUsername(uid_username_cache, comments);
      }); // end $q.all()
    }


    /**
     * Refresh the names in the checks comment (only uuid provided by the API)
     */
     public refreshCommentUsername(uid_username_cache: Map<string, string>, comments: YopComment[][]) {
      console.log("refreshCommentUsername called with uid_username_cache:... ");
      console.log(uid_username_cache);
      for (var checkid in comments ){

        comments[checkid].map(function(current_com) {
          if ('username' in current_com ) {
            //console.log("No need to set the username in the comment")
            } else {
              var current_userid = current_com['comment_userid'];
              var current_username = "anonymous";
              if (current_userid in uid_username_cache) {
                current_username = uid_username_cache[current_userid];
              }
              current_com['username'] = current_username;
            }
            });
      }
    }

    /**
     * Fill gaps in checks 
     */
    public fillGapsInChecks(checks: Check[], stackCreationDate: Date,
                            stackPeriodicity: number, stackFrequency: number, 
                            showMissedPeriods: boolean = true) : Check[] {
      checks = checks.filter((check) => !check.isFrontOnly);
      let currentPeriodCheck = new Check("dummy", new Date().getTime() - 300000, "", "Current period can be checked here !","OK", "");
      currentPeriodCheck.isFrontOnly = true;
      currentPeriodCheck.formattedComment = currentPeriodCheck.comment;
      checks.push(currentPeriodCheck);

      let sortedChecks = checks.sort((a,b) => b.date - a.date);
      let resultChecks = [];
      const referenceDate = stackCreationDate.getTime();
      const currentPeriod = this.calculatePeriod(stackCreationDate.getTime(), stackPeriodicity, (new Date()).getTime());
      let latestCheckPeriod = this.calculatePeriod(stackCreationDate.getTime(), stackPeriodicity, sortedChecks[0].date);

      console.log("Current period is: ", currentPeriod, ",latest check period is: ", latestCheckPeriod);

      let previousCheckPeriod = latestCheckPeriod;
      sortedChecks
      .forEach((check) => {
        let currentCheckPeriod = this.calculatePeriod(referenceDate, stackPeriodicity, check.date);
        let periodDiff = previousCheckPeriod - currentCheckPeriod;
        console.log("PeriodDiff: ", periodDiff , "currentCheckPeriod: ", currentCheckPeriod ,"currentPeriod: ", currentPeriod);
        
        if(showMissedPeriods && periodDiff > 1 && (currentCheckPeriod + periodDiff) <= currentPeriod ) {
          let comment = ""+ periodDiff + " missed periods";
          let latestMissedPeriod = currentCheckPeriod + 1;
          let missedCheck = new Check(currentCheckPeriod.toString(), 
                                      this.periodToDate(latestMissedPeriod, stackCreationDate, stackPeriodicity).getTime(), 
                                      "some_id", comment, CheckStatus[CheckStatus.FAILED], "");
          missedCheck.isFrontOnly = true;
          missedCheck.period = latestMissedPeriod;
          missedCheck.formattedComment = missedCheck.comment;
          resultChecks.push(missedCheck);
        }

        check.period = currentCheckPeriod;
        resultChecks.push(check);
        previousCheckPeriod = currentCheckPeriod;
      })

      // adding a front only check for planning future checks
      const period = Math.max(latestCheckPeriod + 1, currentPeriod + 1);
      let futureCheck = new Check(period.toString(), 
                                  this.periodToDate(period, stackCreationDate, stackPeriodicity).getTime(),
                                  "some_id", "Upcoming checks can be planned here...", CheckStatus[CheckStatus.PLANNED], "");
      futureCheck.isFrontOnly = true;
      futureCheck.formattedComment = futureCheck.comment;
      futureCheck.period = period;

      resultChecks.unshift(futureCheck);

      console.log("Returning checks: ", resultChecks);
      return resultChecks;
    }


    /**
     * Single method to add new checks to the existing list of checks
     * @param {Check[]} checks       [description]
     * @param {Check}   newCheck     [description]
     * @param {Stack}   checkedStack [description]
     */
    public handleNewCheck(checks: Check[], newChecks: Check[], checkedStack: Stack, showMissedPeriods: boolean = true) : Check[] {

      console.log("Adding new check to the list of stack check: ", newChecks);
      
      let updatedChecks = checks.filter(check => !newChecks.map(c=>c.id).includes(check.id));
      // check text formatting
      newChecks.forEach(newCheck => {
        let checkToAdd = new Check(newCheck.id, newCheck.date, newCheck.stack, newCheck.comment, newCheck.status, newCheck.checker);
        checkToAdd.period = this.getPeriod(new Date(checkToAdd.date), new Date(checkedStack.creation_date), checkedStack.periodicity);
        checkToAdd.formattedComment = checkToAdd.comment;
        if(newCheck.formattedComment){
          checkToAdd.formattedComment = newCheck.formattedComment;
        }
        console.log("Handlenewcheck: period: ", checkToAdd.period);
        updatedChecks.push(checkToAdd);
      });

      if (checkedStack.type == StackType.TIME) {
          updatedChecks = this.fillGapsInChecks(updatedChecks, new Date(checkedStack.creation_date),
                                                                        checkedStack.periodicity, checkedStack.frequency,
                                                                        showMissedPeriods);
      } else {
        this.resortChecks(updatedChecks);
      }
      return updatedChecks;
    }

  /**
   * resort the checks
   */
  public resortChecks(checks: Check[]) {
    checks.sort((a,b) => { return b.date - a.date; })
  }

    /**
     * Get the period number based on a reference date and a periodicity
     * @param  {number} referenceDate the reference date
     * @param  {number} periodicity   the stack periodicity in days
     * @param  {number} date          the date for which we want the period
     * @return {number}               the calculated period
     */
    public calculatePeriod(referenceDate: number, periodicity: number, date: number) : number {
      // get the diff in ms between the referenceDate and the date
      let diffToRefMs = date - referenceDate;

      // translate the difference into number of periods
      return Math.floor(diffToRefMs / (oneDayMs * periodicity));
    }

    /**
     * to be deprecated and replaced by calculatePeriod method
    * Returns the numeric value of the period for a given date for a stack
    */
    public getPeriod(date: Date, stack_creation_date: Date, stack_periodicity: number) {
      return Math.floor(((date.getTime() - stack_creation_date.getTime() )/(oneDayMs)) / stack_periodicity);
    }


    public periodToDate(period: number, referenceDate: Date , stackPeriodicity: number) {
      console.log("PeriodToDate: period: ", period, ", referenceDate: ", referenceDate, ", stackPeriodicity: ", stackPeriodicity);
      let result = new Date(referenceDate.getTime() + (period * stackPeriodicity * oneDayMs) + ( 0.5 * stackPeriodicity * oneDayMs ));
      let diff = result.getTime() - referenceDate.getTime();
      console.log("returning: ", result, "date diff: ",  diff/oneDayMs);
      return result;
    }
    

    /*Not use can be removed or re-used for usage with the new implementation*/
    public addFuturPeriodToCheck(checks: Check[], stack_creation_date: Date, stack_periodicity: number, stack_frequency: number) {
      var self = this;
      console.log("adding future check period to checks list ");

      let now:                 Date = new Date();
      let date_in_next_period: Date = new Date(now.getTime() + (oneDayMs * stack_periodicity));
      let period: number = self.getPeriod(date_in_next_period, stack_creation_date, stack_periodicity);
      checks.push(self.getCheckInPeriod(period, stack_creation_date, stack_periodicity, "future", "Future date, plan your checks !", ""));
      console.log("updated check list: ", checks)
    }

    public getCheckInPeriod(period: number, stack_creation_date: Date, stack_periodicity: number, status: any, comment: string, checker: string) : Check {
      let checkDate: Date = new Date(stack_creation_date.getTime() + (oneDayMs * period * stack_periodicity));
      let aCheck: Check =  new Check(period.toString(), checkDate.getTime(), "", comment, status, checker);
      return aCheck;
    }

  }

export class StackTimeDetails {
  public current_period_checked: boolean;
  public start_date: Date;
  public end_date: Date;
  
  constructor(current_period_checked: boolean, start_date: Date, end_date: Date) {
    this.current_period_checked = current_period_checked;
    this.start_date = start_date;
    this.end_date = end_date;
  }


}
export class YopMarked {

  private static instance : YopMarked;

  private renderer;
  private markedInstance;

  private constructor() {
    this.initRenderer();
  }

  static getInstance() : YopMarked {
    if (!YopMarked.instance) {
      YopMarked.instance = new YopMarked();
    }
    return YopMarked.instance;
  }

  public render(input: string) : string {
    return this.markedInstance(input);
  }

    /**
   * Init the marked module with some extra settings to interpret 
   * - [x] as a check mark in list
   *  or in paragraph
   * @return {any} the configured marked module
   */
  private initRenderer() {
      this.renderer = new marked.Renderer();
      this.renderer.listitem = (text) => {
        if (/\s*\[[x ]\]\s*/.test(text)) {
          text = text
          .replace(/\s*\[ \]\s*/, '<i class="empty checkbox icon"></i> ')
          .replace(/\s*\[x\]\s*/, '<i class="checked checkbox icon"></i> ');
              return '<li style="list-style: none">' + text + '</li>';
        } else {
              return '<li>' + text + '</li>';
        }
      };

      this.renderer.paragraph = (text) => {
        if (/\s*\[[x ]\]\s*/.test(text)) {
          text = text
          .replace(/\s*\[ \]\s*/g, '<i class="empty checkbox icon"></i> ')
          .replace(/\s*\[x\]\s*/g, '<i class="checked checkbox icon"></i> ');
        }
        return '<p>' + text + '</p>\n';
      };
      this.markedInstance = marked.options({renderer: this.renderer});
  }
}