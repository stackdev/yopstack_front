import { Component, OnInit, Input } from '@angular/core';
import { User } from '../services/user';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit {

  @Input() user: User;

  constructor() { }

  ngOnInit() {
    
  }

  public getAvatarLetter(user: User) {
    if (undefined === user || user.name === undefined || user.name === "") {
      return "?";
    }
    return user.name.charAt(0).toUpperCase();
  }

  public getAvatarColor(user: User) {

    if (user !== undefined && user.avatar !== undefined && user.avatar.data !== undefined && user.avatar.data !== "") {
      return user.avatar.data;
    }


    if (undefined === user || user.avatar === undefined || user.avatar.data === undefined || user.avatar.data === "") {
      if ( user.name === undefined)
        return "#ff0000";
    }
    var userNameHash = 0;
    // sum char code of each letter of the username
    Array.from(user.name).forEach((c, i) => userNameHash += c.charCodeAt(0))
    // 'normalize' -> not using max value 65xxx but max alphanumerical char value from ascii table ~48 to 172
    let userNameHashNormed = userNameHash / (user.name.length * 172)
    let userNameHexColor: string = `#${(Math.round(userNameHashNormed * 16777215)).toString(16)}`
    console.log(`user color: username: ${user.name}, color: ${userNameHexColor}`)
    return userNameHexColor; 
  }

}
