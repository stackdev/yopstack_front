import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { Router }         from '@angular/router';

import { StackType, StackStatus, GlobalService } from '../../services/global.service';
import { StackService }                          from '../../services/stack.service';
import { Stack }                                 from '../../services/stack';
import { User }                                  from '../../services/user';



@Component({
  selector: 'app-edit-stack',
  templateUrl: './edit-stack.component.html',
  styleUrls: ['./edit-stack.component.css']
})
export class EditStackComponent implements OnInit, OnChanges {

  @Input() stack:         Stack;
  @Input() stack_userid:  string;
  @Input() checkers:      Map<string, User>;
  @Input() isChecked:     boolean;
  @Input() isEditable:    boolean;
  
  @Input() edition:       boolean;
  @Input() deletion:      boolean;

  @Output() onStackEditionCompleted: EventEmitter<any> = new EventEmitter();


  private router: Router;


  private stackService: StackService;
  public global: GlobalService;
  // enum 'types'
  private StackType   = StackType;
  public stackStatus = StackStatus;

  // public for component template access
  public confirm_delete : boolean = false;

  private name_init      : string;
  private desc_init      : string;
  private private_init   : boolean;
  private status_init    : StackStatus;
  private checkers_init     : string[];

  public name_update      : string;
  public desc_update      : string;
  public private_update   : boolean;
  public status_update    : StackStatus;
  public checkers_update  : string[];

  constructor(router: Router, stackService: StackService, global: GlobalService) {
    this.router = router;
    this.stackService = stackService;
    this.global = global;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("onchanges: ", changes);
    if (changes['stack'] && changes['stack'].currentValue != undefined ) {
      //this.getChecksAndComments(new Date().getTime());
      console.log("stack changed: ", changes['stack'].currentValue );
      this.name_init       = this.stack.name;
      this.desc_init       = this.stack.description;
      this.private_init    = this.stack.isPrivate;
      this.checkers_init   = this.stack.checkers;
      this.name_update     = this.stack.name;
      this.desc_update     = this.stack.description;
      this.private_update  = this.stack.isPrivate;
      this.checkers_update = [...this.stack.checkers];
    }
  }

    /**
   * UI: End of edit stack details (title)
   */
   public edition_end() {
     // TODO: compare initial object with a modified one instead of having 
     // one field per attribute to be updated ?
     console.log("data modified ? name " + this.name_init + ", updated name: " + this.name_update);
     console.log("data modified ? checkers_init " + this.checkers_init.length, " checkers_update: " + this.checkers_update.length, "value: ", this.checkers_update);

     if (this.name_init     != this.name_update    ||
         this.desc_init     != this.desc_update    ||
         this.private_init  != this.private_update ||
         this.checkers_init != this.checkers_update ) {
        console.log("data modified, update needed");
      } else {
        console.log("no diffs no updated needed.")
        this.cancel();
        return;
      }

      let stack_modified : Stack = this.stack;
      stack_modified.name = this.name_update;
      stack_modified.description = this.desc_update;
      stack_modified.isPrivate = this.private_update;
      console.log("status_update:", this.status_update, "...>", StackStatus[this.status_update]);
      stack_modified.status = this.status_update;
      stack_modified.checkers = this.checkers_update;

      this.stackService
      .updateAStack(this.stack_userid, stack_modified )
      .subscribe(
        stack => {
          console.log("Stack updated ok: ", stack.checkers.length);
          //this.edition = false;
          this.stack = stack;
          this.onStackEditionCompleted.emit([]);
          },
        () => {
           this.onStackEditionCompleted.emit([]);
        });
    }

    public cancel() {
        console.log("canceling edition...");
        this.onStackEditionCompleted.emit([]);
        return;
    }

    public delete_stack() {
      this.stackService
      .deleteAStack(this.stack_userid, this.stack.id)
      .subscribe(stack => {
                    console.log("stack deleted");
                    //this.edition = false;
                    this.router.navigate(['/']);
                  },
                  () => {
                    this.onStackEditionCompleted.emit([]);
                   //this.edition = !this.edition;
                 });
    }

    public change_status(new_status: string) {
      this.status_update = this.stackStatus[new_status];      
    }

    public onCheckersChange(checkers: Map<string, User>) {
      console.log("on checkers changed: ", checkers);
      this.checkers_update = Array.from(checkers.keys());
    }

} // end