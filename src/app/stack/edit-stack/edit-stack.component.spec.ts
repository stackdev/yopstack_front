import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { EditStackComponent } from './edit-stack.component';
import { GlobalService } from '../../services/global.service';
import { StackService } from '../../services/stack.service';


describe('EditStackComponent', () => {
  let component: EditStackComponent;
  let fixture: ComponentFixture<EditStackComponent>;

  let mockRouter;
  let mockStackService;

  beforeEach(async(() => {
    mockRouter = jasmine.createSpyObj("Router", ["navigate"]);

    mockStackService = jasmine.createSpyObj("StackService", ["updateAStack", "deleteAStack"]);

    TestBed.configureTestingModule({
      declarations: [ EditStackComponent ],
      providers: [
        GlobalService, 
        { provide: Router, useValue: mockRouter },
        { provide: StackService, useValue: mockStackService }
      ],
      schemas: [ NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
