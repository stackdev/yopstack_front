import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation, OnChanges, SimpleChanges} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Check } from '../../services/check';
import { Stack } from '../../services/stack';
import { CheckService } from '../../services/check.service';
import { CommonService } from '../../services/common.service';
import { StackUiLogicService } from '../../services/stack-ui-logic.service';

import { StackType, CheckStatus, EditionMode, GlobalService } from '../../services/global.service';
import { User } from '../../services/user';


@Component({
  selector: 'app-check-stack',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './check-stack.component.html',
  styleUrls: ['./check-stack.component.css']
})
export class CheckStackComponent implements OnInit, OnChanges {


  @Input() stackToBeChecked: Stack;
  @Input() checkDate: Date;
  @Input() checkToBeUpdated: Check;
  @Input() checkers: Map<string, User>;
  @Output() onStackChecked: EventEmitter<any> = new EventEmitter();
  @Output() onCheckAborted: EventEmitter<any> = new EventEmitter();
  
  //public checkerIdMap: Map<string, string> = new Map([["dzd", "dzd_id"],["plop", "plop_id"]]);

  public StackType = StackType;

  public check_date: any;

  public availableCheckStatus: string[];
  public newCheckStatus: string;

  public futureCheckStatusOptionList: string[] = [ CheckStatus[CheckStatus.PLANNED] ];
  public pastCheckStatusOptionList: string[] =   [ CheckStatus[CheckStatus.OK], 
                                                   CheckStatus[CheckStatus.FAILED], 
                                                 CheckStatus[CheckStatus.FAILED], 
                                                   CheckStatus[CheckStatus.FAILED], 
                                                   CheckStatus[CheckStatus.SKIPPED] ];
  public defaultCheckStatus = {[StackType.KANBAN]  : CheckStatus.PLANNED,
                               [StackType.TIME]    : CheckStatus.OK,
                               [StackType.NOTES]   : CheckStatus.OK,
                               [StackType.SHARED]  : CheckStatus.OK}

  public editionMode: EditionMode;
  public editedCheck: Check;

  public editedCheckUpdatedUserId: string;

  constructor(private checkService: CheckService, 
              private commonService: CommonService,
              private stackUiLogicService: StackUiLogicService,
              private globalService: GlobalService) {
  }

  ngOnInit() {
    if(this.checkToBeUpdated) {
      this.editionMode = EditionMode.UPDATE;
      this.editedCheck = this.checkToBeUpdated;
      this.newCheckStatus = this.editedCheck.status;
      this.editedCheckUpdatedUserId = this.checkToBeUpdated.checker;
    } else {
      this.newCheckStatus = this.defaultCheckStatus[this.stackToBeChecked.type];
      this.editionMode = EditionMode.CREATION;
      this.editedCheck = new Check(this.stackToBeChecked.id, this.checkDate.getTime(), this.stackToBeChecked.id,
                                   "", this.newCheckStatus, this.globalService.authenticatedUser.id);
    }
    this.check_date = new FormControl(new Date(this.editedCheck.date));
    this.selectedDateChanged(new Date(this.editedCheck.date));
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("onchanges: ", changes);
    if (changes['checkDate'] && changes['checkDate'].currentValue != undefined ) {
      console.log("Value changed for checkDate: ", changes['checkDate'].currentValue );
    }     
  }

  public abort() {
      this.onCheckAborted.emit(["aborted"]);
  }

  public checkit() {
      // backup check text in case of save failure
      let check_date = this.check_date.value;
      //if date is in the past, set check hour at 12:00
      let check_date_date = new Date(this.check_date.value);
      if( check_date_date.getHours() == 0 && check_date_date.getMinutes() == 0) {
        this.commonService.addHours(check_date_date, 12);
      }

      let oldText = "";


      if (this.editedCheck.comment) {
         this.editedCheck.date = check_date_date.getTime();
         // Kind of a workaround since default KANBAN check is 'PLANNED' 
         // but the backend doesn't accept planned checks in the past...
         if(this.stackToBeChecked.type === StackType.KANBAN) {
          this.editedCheck.date = this.commonService.addHours(new Date(), 1).getTime();
          this.editedCheck.checker = this.editedCheckUpdatedUserId;
         }
         this.editedCheck.status = CheckStatus[this.newCheckStatus];

         let obs;
         if (this.editionMode == EditionMode.CREATION) {
           obs = this.checkService.createACheck(this.editedCheck);
         } else {
           obs = this.checkService.updateACheck(this.editedCheck);
         }
         
         obs.subscribe( 
            checkEdited => {
              // setting period in check
              checkEdited.period = this.stackUiLogicService.getPeriod(new Date(checkEdited.date), new Date(this.stackToBeChecked.creation_date),
                                                 this.stackToBeChecked.periodicity);
              console.log("Edited check: ", checkEdited);
              this.onStackChecked.emit([checkEdited]);
            },
            (error) => {
                console.log("edition failed, restoring check text...", error);
                if(this.editionMode == EditionMode.UPDATE) {
                  this.editedCheck.comment = oldText; 
                }
                this.onCheckAborted.emit(["aborted"]);
            });
      } 
  }

  public selectedDateChanged(newDate: Date) {
    console.log("Selected Date changed: ", newDate);
    if (newDate.getTime() > (new Date()).getTime() + 5 * 60 * 100) {
      console.log("New Date is in future, update the checkstatus option list to: ", this.futureCheckStatusOptionList);
      this.availableCheckStatus = this.futureCheckStatusOptionList;
    } else {
      console.log("New Date is in future, update the checkstatus option list to: ", this.pastCheckStatusOptionList);
      this.availableCheckStatus = this.pastCheckStatusOptionList;
    }
    // In creation mode
    // if the status is not compatible with the list, force it to the first value in the array
    if (this.stackToBeChecked.type === StackType.TIME ) {
      if (this.editionMode == EditionMode.CREATION &&
        !this.availableCheckStatus.includes(this.newCheckStatus)) {
        console.log("Forcing newCheckStatus to ", this.availableCheckStatus[0] );
        this.newCheckStatus = this.availableCheckStatus[0];
      } else {
        // UPDATE mode, add the current value to the available show_option_list
        if(!this.availableCheckStatus.includes(this.newCheckStatus)) {
        this.availableCheckStatus.unshift(this.newCheckStatus);
        }
      }
    }
  }

  public tmp() {}

} // end