import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CheckStackComponent } from './check-stack.component';

import { CheckService } from '../../services/check.service';
import { CommonService } from '../../services/common.service';


describe('CheckStackComponent', () => {
  let component: CheckStackComponent;
  let fixture: ComponentFixture<CheckStackComponent>;

  let mockCheckService;

  beforeEach(async(() => {
    mockCheckService = jasmine.createSpyObj("CheckService", ["createACheck"]);

    TestBed.configureTestingModule({
      declarations: [ CheckStackComponent ], 
      providers: [
        CommonService,
        { provide: CheckService, useValue: mockCheckService }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
