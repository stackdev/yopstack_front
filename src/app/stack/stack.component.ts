import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router }       from '@angular/router'; 
import { Observable }                   from 'rxjs/Observable';
import { merge }                        from 'rxjs';

import { GlobalService, StackType }       from '../services/global.service';
import { UserService }         from '../services/user.service';
import { StackService }        from '../services/stack.service';
import { CheckService }        from '../services/check.service';
import { CommentService }      from '../services/comment.service';
import { PreferenceService }   from '../services/preference.service';
import { ContextDataService }  from '../services/context-data.service';
import { StackUiLogicService, YopMarked} from '../services/stack-ui-logic.service';

import { User }              from '../services/user';
import { Stack }             from '../services/stack';
import { Check }             from '../services/check';
import { Preference }        from '../services/preference';
import { StackPreference }   from '../services/stackPreference';
import { YopComment }        from '../services/comment';





@Component({
  selector: 'app-stack',
  templateUrl: './stack.component.html',
  styleUrls: ['./stack.component.css']
  })
export class StackComponent implements OnInit, OnDestroy {

  private sub:             any;
  private stack_id:        string;

  // public for component template access
  public StackType = StackType;
  public displayed_stack: Stack;
  public user_id:         string;
  public current_period_checked: boolean = false;
  public isOwner: boolean = false;
  public isChecker: boolean = false;
  public new_check: Check = null;
  public stack_owner:     User;
  public next_period_check_start_date: Date = new Date();
  public next_period_check_end_date: Date = new Date();

  // different views triggers
  public checkActive:      boolean = false;
  public stats_active:     boolean = false;
  public kanban_active:    boolean = false;
  public list_active:      boolean = false;
  public preferenceActive: boolean = false;

  // edition / deletion
  public edition: boolean = false;
  public edit_visible : boolean = false;

  public deletion: boolean = false;

  public checkerNames : Set<string> = new Set();
  public checkers : Map<string, User>;

  public newCheckDefaultDate: Date = new Date();

  public stackPreference: StackPreference;


  // checks and comments data moved back from list-check component
  public checks: Check[] = [];
  public comments: YopComment[][] = [];


  constructor(private route: ActivatedRoute, private router: Router, private globalService: GlobalService,
    private userService: UserService, private stackService: StackService, private checkService: CheckService,
    private stackUiLogicService: StackUiLogicService, private contextDataService: ContextDataService,
    private commentService: CommentService, private preferenceService: PreferenceService) {
  }

  ngOnInit() {
    // Setting the context
    this.contextDataService.changeContext("stack_context_readonly");

    // Subscribing to the contextual action changes
    this.contextDataService.currentAction.subscribe(
        new_action => {
            
            // only resetting the action for kanban and list. since the other are modals.
            if (["action_kanban", "action_list"].includes(new_action)) {
              this.resetActiveContext();
            }

            console.log("New action requested: " + new_action);
            if (new_action === "action_check") {
                console.log("Action check requested, go for it");
                this.checkActive = true;
              } else if (new_action === "action_stats") {
                this.stats_active = true;
              } else if (new_action === "action_kanban") {
              this.kanban_active = true;
              } else if (new_action === "action_list") {
                this.list_active  = true;
              }  else if (new_action === "action_preference") {
                this.preferenceActive = true;
            }

            console.log("checkActive: ", this.checkActive, ", list_active: ", this.list_active,
                        ", stats_active: ", this.stats_active, ", kanban_active: ", this.kanban_active);
    });

    this.route.params
    .subscribe(params => {
      this.stack_id = params['sid'];
      this.user_id  = params['uid'];
      console.log("uid: ", this.user_id, ", sid: ", this.stack_id);


      this.userService.getAUserById(this.user_id)
      .subscribe(
        user => {
          this.stack_owner = user;
          if (user.name == this.globalService.authenticatedUser.name ) {
            this.isOwner = true;
            this.contextDataService.changeContext("stack_context");
          }
           console.log("stack owner name: ", this.stack_owner.name);
        },
        () => { console.log("Retrieval of user failed");}
      );

     this.stackService.getAStackById(this.user_id, this.stack_id)
     .subscribe(
        stack => {
          this.displayed_stack = stack;
          console.log("displayed_stack:: ", this.displayed_stack, "authenticatedUser id: ", this.globalService.authenticatedUser.id);
          if( this.displayed_stack.checkers.includes(this.globalService.authenticatedUser.id)) {
            this.isChecker = true;
            this.contextDataService.changeContext("stack_context");
          }

          // TMP here...
          // this should be populated from the actually userPreference stored in global service (see fromPreference static method)
          if(!this.globalService.userPreference) {
            console.log("No preference, setting default values...");
            this.stackPreference = new StackPreference(this.displayed_stack.id, true, true, false, true);
          } else {
            console.log(this.globalService.userPreference);
            this.stackPreference = this.getStackPreferenceFromPreference(this.displayed_stack.id, this.globalService.userPreference);
          }
          //console.log("preferences: ", this.stackPreference, "global pref: ", this.globalService.userPreference);

          //updating the current period status 
          this.updateNextCheckPeriod(this.displayed_stack.frequency,
          this.displayed_stack.periodicity,
          new Date(this.displayed_stack.last_check),
          new Date(this.displayed_stack.creation_date));

          //update list of checkers (should be done by chaining it to the get stack observable but not sure yet how to do that...)
          
          // retrieve checks and comments
          this.getChecksAndComments(Math.max(this.displayed_stack.last_check, this.displayed_stack.last_planned_check));

          this.refreshCheckerNames();

          },
        () =>{ console.log("Retrieval of stack failed");}
      );


   });
  }

  public ngOnDestroy() {
    console.log("stack component destroyed, reset action context");
    this.resetActiveContext();
    this.contextDataService.changeAction("");
  }

  /**
   * 
   */
  private resetActiveContext() {
      this.stats_active = false;
      this.checkActive = false;
      this.list_active  = false;
      this.preferenceActive = false;
      this.kanban_active = false;
  }

    /**
    * Update next check period
    */
    public updateNextCheckPeriod(frequency: number, periodicity: number, last_check: Date, creation_date: Date) {
      var periodDate = this.stackUiLogicService.computeNextCheckDate(frequency, periodicity, last_check, creation_date);
      console.log(this.current_period_checked);
      this.next_period_check_start_date = periodDate.start_date;
      this.next_period_check_end_date   = periodDate.end_date;
      this.current_period_checked       = periodDate.current_period_checked;
      console.log(this.current_period_checked);

      //StackService.timeBeforeDate(new Date(), new Date(periodDate.end_date));
    }

    public refreshCheckerNames() {

      let allCheckerId = this.displayed_stack.checkers;
      allCheckerId.push(...this.displayed_stack.admins);

      let checkersObservable: Observable<User>[] = [];
      allCheckerId
      .forEach(uid => {
        checkersObservable.push(this.userService.getAUserById(uid)); 
      });

      let mergedCheckersObservables = merge(...checkersObservable);
      mergedCheckersObservables
      .toArray()
      .subscribe(checkers => {
                  console.log("working on checkers: ", checkers);
                  this.checkers = new Map(checkers.map(key => [key.id, key] as [string, User]));  
                  this.checkerNames = new Set(Array.from(this.checkers.values()).map(u=>u.name));  
                  console.log("checkers: ", this.checkerNames);
                  })
    }

    /**
     * Triggering of the stats component
     */
     public get_stats() {
      this.router.navigate(["/stats", this.displayed_stack.id ] );
    }


    /**
     * Toggle stack edition
     */
     public toggle_edition() {
      this.edition =!this.edition;
    }

     /**
     * Toggle stack edition
     */
     public toggle_delete() {
      this.deletion =!this.deletion;
    }

    /**
     * On stack edition/update completed
     */
     public OnStackEditionCompleted() {
      this.edition = false;
      this.edit_visible = false;
      this.refreshCheckerNames();
    }

    public onCheckCompleted(newCheck: Check) {
      this.new_check = newCheck;
      this.checkActive = false;
      let formattedCheck = this.reloadCheckFormattedText([newCheck]);
      console.log("Formatted Check: ", formattedCheck, "was: ", newCheck);
      this.checks = this.stackUiLogicService.handleNewCheck(this.checks, formattedCheck, this.displayed_stack,
                                                            this.stackPreference.showMissedPeriods);
    }

    public onCheckAborted(event: any) {
      console.log("Check aborted.", event);
      this.checkActive = false;
    }

   public onPreferenceUpdateCompleted(preference: StackPreference) {
      this.preferenceActive = false;
      if (preference) {
        if( preference.enableMarkdownInComment != this.stackPreference.enableMarkdownInComment ||
            preference.enableMarkdownInCheck   != this.stackPreference.enableMarkdownInCheck ||
            preference.showMissedPeriods       != this.stackPreference.showMissedPeriods ||
            preference.showPeriodInCheck       != this.stackPreference.showPeriodInCheck) {
          console.log("Preference: update required!");
          const updatedPreference = this.getUpdatedPreferenceFromStackPreference(this.globalService.userPreference, preference);
          this.preferenceService.updateAPreference(updatedPreference)
          .subscribe(
            preference => {
              this.globalService.userPreference = preference;
              console.log("preference updated successfully.")},
            error=>console.log("preference update faile..."))
        } else {
          console.log("Preference: no update required.", this.stackPreference, preference);
        }
        this.stackPreference = preference;
        this.reloadCheckFormattedText(this.checks);
        this.reloadCommentFormattedText(this.comments);
        this.checks = this.stackUiLogicService.fillGapsInChecks(this.checks, new Date(this.displayed_stack.creation_date),
                                                                this.displayed_stack.periodicity,
                                                                this.displayed_stack.frequency,
                                                                preference.showMissedPeriods);
      }
    }


  /*
  Getting checks and comments before the specified date
  */
  public getChecksAndComments(before_date: number) {
    // TODO: loading spinner stays incomments list checks for now
    //this.checksLoaded = false;
    // add one millisecond to provided date as the backend does strickly before search
    before_date = before_date + 1;
    console.log("getting checks for this stack: ", this.displayed_stack.id, "before_date: ", before_date);

    let checksIds = this.checks.map(check=>check.id);

    this.checkService
    .getChecksBetweenDates(this.displayed_stack.id, before_date.toString(), undefined)
    .flatMap(check => check)
    .toArray()
    .subscribe(checks => {
      //this.checksLoaded = true;
      this.checks = this.stackUiLogicService.handleNewCheck(this.checks, checks, this.displayed_stack, this.stackPreference.showMissedPeriods);
      // TODO do this call only if we got new checks (bug...)
      this.commentService.getCommentsForChecks(this.user_id, this.displayed_stack.id, checks.map(check=> check.id))
      .subscribe(comments => {
          this.addCommentsToCheckCommentList(comments, this.comments); }
      );
      // TMP here : 
      this.checks = this.reloadCheckFormattedText(this.checks);
      this.comments = this.reloadCommentFormattedText(this.comments);
    }, 
    error => {/*this.checksLoaded = true;*/}
    );
  }

  public reloadCheckFormattedText(checks: Check[]) {
    let markdownFormatting = false;
    let showPeriod = false;
    let markedRenderer = YopMarked.getInstance();
    if(this.stackPreference){
      if(this.stackPreference.enableMarkdownInCheck) {
        markdownFormatting = true;
        console.log("Markdown formatting requested, for checks.");
      }
      if (this.stackPreference.showPeriodInCheck) {
        showPeriod = true;
        console.log("Period in checks requested.");
      }
    } 
    checks.forEach(check => { 
      let prefix = "";
      if (showPeriod) {
        prefix = `[ period ${check.period} ] `;
      }
      let comment = prefix + check.comment;
      if (markdownFormatting) {
        comment = markedRenderer.render(comment)
      } 

      check.formattedComment = comment;
    }); 
    return checks;
  }

  public reloadCommentFormattedText(comments: YopComment[][]) {
    if(!this.stackPreference || !this.stackPreference.enableMarkdownInComment) {
      console.log("No Markdown formatting required, for comments.", comments);
      for(let id in comments) {
        comments[id].forEach(comment => {
          comment.text_formatted = comment.text
        });
      }
     } else {
        let markedRenderer = YopMarked.getInstance();
        for(let id in comments) {
          comments[id].forEach(comment => {
            comment.text_formatted = markedRenderer.render(comment.text)
          })
        }
    }
    console.log("Returning: ", comments);
    return comments;
  }


  /**
   * add comments to the existing data structure
   * @param {YopComment[]}   newComments comments to be added
   * @param {YopComment[][]} comments    data structure (check - comments)
   */
  public addCommentsToCheckCommentList(newComments: YopComment[], comments: YopComment[][]) {
    if(!newComments) { return }
    console.log("BUGFIX: ", newComments , "->", comments);
    
    let allCommentIds = new Set();
    for(let checkid in comments) {
      console.log("BUGFIX: ",checkid);
      comments[checkid].map(c => {
        allCommentIds.add(c.id) 
      });
    }
    console.log("BUGFIX: all comment ids", allCommentIds);
    newComments.
    map((comment) => {
        if ( comments[comment.checkid] == undefined ) {
           comments[comment.checkid] = [];
        }
        console.log("Add comment for checkid: ", comment.checkid);

        // markdown processing - could be triggered by a parameter in comment if needed
        comment.text_formatted = comment.text;
        if(this.stackPreference.enableMarkdownInComment) {
          comment.text_formatted = YopMarked.getInstance().render(comment.text);
        }
        comments[comment.checkid].push(comment);
      });
    this.stackUiLogicService.setUserNameInComments(comments);
  }

  public onMoreCheckRequested(beforeDate: number) {
    this.getChecksAndComments(beforeDate);
  }

  public onMoreCommentRequested(checkId: string) {
    // get last check date and request older comments
    let last_comment_date = this.comments[checkId].slice(-1)[0].timestamp || 0;
    // this.comments[checkId]
    // .map((comment) => {
    //   if ((comment.timestamp && comment.timestamp < last_comment_date ) || 
    //       last_comment_date == 0 ) {
    //     last_comment_date = comment.timestamp;
    //   }
    // });

    console.log("getting comments between: ", last_comment_date, " and ", this.displayed_stack.creation_date);
    this.commentService.getCommentsBetweenDates(this.user_id, this.displayed_stack.id, checkId, 
                                                new Date().getTime().toString(), 
                                                last_comment_date.toString())
    .subscribe(
      comments => {
        this.addCommentsToCheckCommentList(comments, this.comments);
        console.log("... comments: " , this.comments);
        }, 
        ()  => { 
          console.log("get comment failed") 
        }
      );
  }

  public getStackPreferenceFromPreference(stackId: string, preference: Preference): StackPreference {

    let disableMissedPeriod = preference.disableMissedPeriod || [];
    let displayCheckPeriod = preference.displayCheckPeriod || [];
    let disableMarkdownInCheck = preference.disableMarkdownInCheck || [];
    let disableMarkdownInComment = preference.disableMarkdownInComment || [];

    let pref = new StackPreference(stackId, 
          !disableMissedPeriod.includes(stackId),
           displayCheckPeriod.includes(stackId),
          !disableMarkdownInCheck.includes(stackId),
          !disableMarkdownInComment.includes(stackId));
    return pref;
  }

  // TODO: add test coverage with a parametrized test (like for getStackPreferenceFromPreference)
  // then refactor...
  public getUpdatedPreferenceFromStackPreference(userPreference: Preference, stackPreference: StackPreference) {
    const stackId = stackPreference.stackId;
    let pref = new Preference(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
    Object.assign(pref, userPreference);

    const disableMarkdownInCheckIndex = pref.disableMarkdownInCheck.indexOf(stackId);
    if (!stackPreference.enableMarkdownInCheck) {
      disableMarkdownInCheckIndex === -1 ? pref.disableMarkdownInCheck.push(stackId): undefined;
    } else {
      disableMarkdownInCheckIndex === -1 ? undefined: pref.disableMarkdownInCheck.splice(disableMarkdownInCheckIndex, 1);
    }

    const disableMarkdownInCommentIndex = pref.disableMarkdownInComment.indexOf(stackId);
    if (!stackPreference.enableMarkdownInComment) {
       disableMarkdownInCommentIndex === -1 ? pref.disableMarkdownInComment.push(stackId): undefined;
    } else {
       disableMarkdownInCommentIndex === -1 ? undefined: pref.disableMarkdownInComment.splice(disableMarkdownInCommentIndex, 1);
    }

    const disableMissedPeriodIndex = pref.disableMissedPeriod.indexOf(stackId);
    if (!stackPreference.showMissedPeriods) {
       disableMissedPeriodIndex === -1 ? pref.disableMissedPeriod.push(stackId): undefined;
    } else {
       disableMissedPeriodIndex === -1 ? undefined: pref.disableMissedPeriod.splice(disableMissedPeriodIndex, 1);
    }

    const displayCheckPeriodIndex = pref.displayCheckPeriod.indexOf(stackId);
    if (stackPreference.showPeriodInCheck) {
      displayCheckPeriodIndex === -1 ? pref.displayCheckPeriod.push(stackId): undefined;
    } else {
      displayCheckPeriodIndex === -1 ? undefined: pref.displayCheckPeriod.splice(displayCheckPeriodIndex, 1);
    }
    return pref;
  }

  public getActionOrder(stack: Stack) : string[]{
    
    if (stack === undefined)
      return [];
    switch (stack.type){
       case StackType.KANBAN:
        return ["action_kanban", "action_list", "action_preference", "action_check"];
      case StackType.NOTES:/*  */
        return ["action_list", "action_preference", "action_check"];
      case StackType.TIME:
      default:
        return ["action_list", "action_stats", "action_preference", "action_check"];
    }
  }

}
