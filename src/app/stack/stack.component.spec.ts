import { async, ComponentFixture, TestBed , inject} from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Observable } from 'rxjs/Observable';


import { StackComponent } from './stack.component';
import { EditFieldComponent } from '../components/edit-field/edit-field.component';


import {ActivatedRoute, Router} from '@angular/router';

import { GlobalService} from '../services/global.service'
import { UserService} from '../services/user.service'
import { StackService} from '../services/stack.service'
import { CommentService} from '../services/comment.service'
import { CheckService} from '../services/check.service'
import { StackUiLogicService} from '../services/stack-ui-logic.service'
import { ContextDataService} from '../services/context-data.service'

import { Check } from '../services/check';
import { Preference } from '../services/preference';
import { StackPreference } from '../services/stackPreference';

let mockActivatedRoute: ActivatedRoute;
let mockRouter: Router;

// our own services
let mockGlobalService;
let mockUserService;
let mockStackService;
let mockCheckService;
let mockCommentService;
let mockStackUiLogicService;
//let mockContextDataService: ContextDataService;


describe('StackComponent', () => {
  let component: StackComponent;
  let fixture: ComponentFixture<StackComponent>;

  beforeEach(async(() => {
    mockGlobalService = {'authenticatedUser': 'plop'};
    mockActivatedRoute = jasmine.createSpyObj('ActivatedRoute', ['params']);
    mockActivatedRoute.params = Observable.create(observer => { observer.next({"name":""});observer.complete(); });

    mockUserService = jasmine.createSpyObj('UserService', ['getAUserById']);
    mockUserService.getAUserById.and.callFake( (user) => { return Observable.create(observer => { observer.next([{"name":""}]);observer.complete(); });})

    mockStackService = jasmine.createSpyObj('StackService', ['getAStackById']);
    mockStackService.getAStackById.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":"", "checkers":["plop", "plip"]});observer.complete(); });})


    mockCommentService = jasmine.createSpyObj('CommentService', ['getCommentsForChecks']);
    mockCommentService.getCommentsForChecks.and.callFake( (comments) => { return Observable.create(observer => { observer.next({});observer.complete(); });})


    mockStackUiLogicService = jasmine.createSpyObj('StackUiLogicService', ['computeNextCheckDate', 'fillGapsInChecks']);
    mockStackUiLogicService.computeNextCheckDate.and.callFake(()=>0.0);

    TestBed.configureTestingModule({
      declarations: [ StackComponent ],
      providers: [
        {provide: Router, useValue: mockRouter},
        {provide: ActivatedRoute, useValue: mockActivatedRoute},
        {provide: GlobalService, useValue: mockGlobalService},
        {provide: UserService, useValue: mockUserService},
        {provide: StackService, useValue: mockStackService},
        {provide: CommentService, useValue: mockCommentService},
        {provide: CheckService, useValue: mockCheckService},
        {provide: StackUiLogicService, useValue: mockStackUiLogicService},
        //{provide: ContextDataService, useValue: mockContextDataService},
        ContextDataService
       ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    console.log("Component ? ", component);
    try {
    expect(component).toBeTruthy();
    } catch(e) {
      console.log("plop", e);
    } 
  });

  //getStackPreferenceFromPreference
  //fit('getStackPreferenceFromPreference should handle not initialized preferences', () => {
    [
      {tid: 1, displayCheckPeriod: undefined, disableMissedPeriod: undefined,  disableMarkdownInCheck: undefined, disableMarkdownInComment: undefined, 
               expShowPeriodInCheck:   false, expShowMissedPeriods:     true, expEnableMarkdownInCheck:     true, expEnableMarkdownInComment:     true},
      {tid: 2, displayCheckPeriod:        [], disableMissedPeriod:        [],  disableMarkdownInCheck:        [], disableMarkdownInComment:         [], 
               expShowPeriodInCheck:   false, expShowMissedPeriods:     true, expEnableMarkdownInCheck:     true, expEnableMarkdownInComment:    true},
      {tid: 3, displayCheckPeriod: ["2","3"], disableMissedPeriod: ["1","2"],  disableMarkdownInCheck: ["2","3"], disableMarkdownInComment:  ["1","2"], 
               expShowPeriodInCheck:   false, expShowMissedPeriods:    false, expEnableMarkdownInCheck:     true, expEnableMarkdownInComment:   false},
    ].forEach(({tid, displayCheckPeriod,   disableMissedPeriod,  disableMarkdownInCheck,   disableMarkdownInComment,
                expShowPeriodInCheck, expShowMissedPeriods, expEnableMarkdownInCheck, expEnableMarkdownInComment}) => {

      it(`getStackPreferenceFromPreference: ${tid}`, () => {
        let stackId = "1";
        let preference: Preference = new Preference(stackId, null, null, null,  displayCheckPeriod, disableMissedPeriod,
                                                                   disableMarkdownInCheck, disableMarkdownInComment);
        let expectedPreference: StackPreference = new StackPreference(stackId, expShowMissedPeriods, expShowPeriodInCheck,
                                                                     expEnableMarkdownInCheck, expEnableMarkdownInComment);
        try { 
          expect(expectedPreference).toEqual(component.getStackPreferenceFromPreference(stackId, preference));
        } catch(e) {
          fail("Failed");
        } 
      });
    });
  //});
});


