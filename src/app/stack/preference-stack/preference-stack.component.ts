import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { StackType} from '../../services/global.service';
import { StackPreference} from '../../services/stackPreference';


@Component({
  selector: 'app-preference-stack',
  templateUrl: './preference-stack.component.html',
  styleUrls: ['./preference-stack.component.css']
})
export class PreferenceStackComponent implements OnInit, OnChanges {


  @Input() stackType: StackType;
  @Input() stackPreference: StackPreference;

  @Output() onPreferenceUpdateCompleted: EventEmitter<any> = new EventEmitter();

  public showMissedPeriods: boolean = true;
  public showPeriodInCheck: boolean = false;
  public enableMarkdownInCheck: boolean = true;
  public enableMarkdownInComment: boolean = true;

  public StackType = StackType;

  constructor() {
  }

  ngOnInit() {
    console.log("StackType: ", this.stackType);
  }

    ngOnChanges(changes: SimpleChanges) {

    if (changes['stackPreference'] && 
        changes['stackPreference'].currentValue != undefined ) {
      this.showMissedPeriods = this.stackPreference.showMissedPeriods;
      this.showPeriodInCheck = this.stackPreference.showPeriodInCheck;
      this.enableMarkdownInCheck = this.stackPreference.enableMarkdownInCheck;
      this.enableMarkdownInComment = this.stackPreference.enableMarkdownInComment;
    }
  }


  public abort() {
    console.log("Aborting preferences edition.");
    this.onPreferenceUpdateCompleted.emit([undefined]);
  }

  public save() {
    console.log("Saving stack preferences.");
    // TODO: check if changed 
    // TODO: call the backend to save new value 
    // emit the new stackPreference if changed
    let newStackPreference: StackPreference = new StackPreference(this.stackPreference.stackId, this.showMissedPeriods, 
                                                                  this.showPeriodInCheck, this.enableMarkdownInCheck,
                                                                  this.enableMarkdownInComment);
    this.onPreferenceUpdateCompleted.emit([newStackPreference]);

  }

}
