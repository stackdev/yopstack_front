import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreferenceStackComponent } from './preference-stack.component';

describe('PreferenceStackComponent', () => {
  let component: PreferenceStackComponent;
  let fixture: ComponentFixture<PreferenceStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreferenceStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreferenceStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
