import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { CheckService }                                       from '../../services/check.service';
import { Check }                                              from '../../services/check';
import { Stack }                                              from '../../services/stack';
import { CommentService }                                     from '../../services/comment.service';
import { YopComment }                                         from '../../services/comment';
import { StackUiLogicService, YopMarked}                      from '../../services/stack-ui-logic.service';
import { GlobalService, StackType }                           from '../../services/global.service';
import { Observable }                                         from 'rxjs/Observable';
import { zip, forkJoin}                                       from 'rxjs';
import { concatMap}                                           from 'rxjs/operators';
import { FormControl }                                        from '@angular/forms';


@Component({
  selector: 'app-list-check',
  templateUrl: './list-check.component.html',
  styleUrls: ['./list-check.component.css']
  })
export class ListCheckComponent implements OnInit, OnChanges {

  @Input() public displayed_stack: Stack;
  @Input() public stack_userid: string;
  @Input() public new_check: Check;
  @Input() public isOwner: boolean = false;
  @Input() public isChecker: boolean= false;

  @Input() public checks: Check[];
  @Input() public comments: YopComment[][];

  @Output() onStackChecked: EventEmitter<any> = new EventEmitter();
  @Output() moreCheckRequested: EventEmitter<any> = new EventEmitter();
  @Output() moreCommentRequested: EventEmitter<any> = new EventEmitter();
  
  public StackType = StackType;

  public authenticated_user:    string; 
  public authenticated_user_id: string; 

  // data
  //public comments:              YopComment[][] = [];
  public new_comment_text:      string = "";
  private more_comment:         boolean     = false;
  public displayedCheckId:      string;
  private more_check_available: boolean     = true;


  // public for component template access
  //component states
  public checksLoaded:         boolean    = false;
  // displayed list of checks, populated with this.data
  
  //public checks:               Check[]     = [];
  public checks_ids:           string[]    = [];

  public current_check_date_edition : string = "";
  public check_date_edited : any;

  // WIP check submission refactoring
  public checkActive: boolean = false;
  public checkPeriodDate: Date;
  public now: number = new Date().getTime();
  public checkForUpdate: Check;

  constructor(private globalService: GlobalService, private commentService: CommentService, 
              private checkService: CheckService, private stackUiLogicService: StackUiLogicService) {}

  ngOnInit() {
      this.authenticated_user    = this.globalService.authenticatedUser.name;
      this.authenticated_user_id = this.globalService.authenticatedUser.id;
  }

  ngOnChanges(changes: SimpleChanges) {

    console.log("onchange: stack_userid:", this.stack_userid, changes);
    if(changes['checks'] && changes['checks'].currentValue != undefined ) {
      console.log("checks updated");
      this.checksLoaded = true;
    }
  }

  /**
  * UI, display check details
  */
  public showCheckDetails(id: string) {

    if( this.displayedCheckId !== id ) {
      this.displayedCheckId = id;
    } else {
      this.displayedCheckId = undefined;
    }
  }

    /**
    * Getting remaining comments before the oldest one in current check
    */
    public getMoreComment(checkid: string) {

      console.log("More comments requested");
      this.moreCommentRequested.emit([checkid]);
    }

    /**
     * [areMoreChecksAvailable description] return true if more checks exists in
     * the backend
     */
    public areMoreChecksAvailable() {

      if (!this.checks || !this.displayed_stack) { return false; }

      let checksLen = this.checks.filter(c => ! c.isFrontOnly).length;
      console.log(`check visibles: ${checksLen}, total:  ${this.displayed_stack.checkCount.total}`);
      return this.displayed_stack && 
             checksLen < this.displayed_stack.checkCount.total &&
             this.checksLoaded;
    }

    public getMoreChecks() {
      this.checksLoaded = false;
      // search oldest check <> last item in checks array
      let oldestCheck = this.checks
        .filter(check => !check.isFrontOnly)  
        .slice(-1)[0]

      console.log("More checks requested, before date: ", oldestCheck.date); 
      this.moreCheckRequested.emit([oldestCheck.date]);
    }


    public editCheck(check: Check) {
      this.checkActive = true;
      this.checkForUpdate = check;
    }

    /**
     * hook for check text and date edition (text == comment)
     * @param {Check} check [description]
     */
    public start_check_edition(check: Check) {
      this.current_check_date_edition = check.id;
      this.check_date_edited =  new FormControl(new Date(check.date));
      console.log("backup current date: ", this.check_date_edited);
    }

    /**
     * hook for check text and date edition cancellation
     * @param {Check} check [description]
     */
    public cancel_check_edition(check: Check) {
      this.current_check_date_edition = "";
      this.check_date_edited =  new FormControl(new Date(check.date));
    }

    public delete_check(check_to_delete: Check) {
      console.log("Deleting check: ", check_to_delete);
      let self = this;
      this.checkService.deleteACheck(check_to_delete).subscribe(
        deleted_check => {
          console.log("deletion ok", deleted_check);
          self.checks = self.checks.filter(c => {return c.id !== deleted_check.id;});
        },
        () => {
          console.log("deletion failed for check: ", check_to_delete); 
        } 
        );
    }


    public update_comment_text(comment_org: YopComment, modified_comment_text: string) {
     let comment_modified : YopComment = comment_org;
     comment_modified.text = modified_comment_text;
     comment_modified.update_timestamp = new Date().getTime();

     this.commentService.updateAComment(comment_modified).subscribe(
      comment => {
        //comment_modified.text_formatted =
        comment_modified.text_formatted = YopMarked.getInstance().render(comment_modified.text);
        console.log("comment update ok");
      },
      () => {
        console.log("comment update failed");
        this.comments[comment_org.checkid].map(c =>{
          if(c.id == comment_org.id ) {
            c.text = comment_org.text;
          }
        });
      });
   }

  /**
  * insert the new comment in backend and in the component data structure
  * @param {[type]} checkId          id of the check 
  * @param {[type]} stackUserId      user id
  * @param {[type]} displayedStackId stack id
  */
  public insertCommentToCheck(checkId, stackUserId, displayedStackId) {

    console.log("insertCommentToCheck: ", checkId, this.new_comment_text, this.authenticated_user_id);
    if (!this.new_comment_text) {
      console.log("Comment insertion cancelled as the text can not be empty or null");
      return;
    }
    if (!this.authenticated_user_id) {
      console.log("Comment insertion cancelled the authenticated user is not defined.");
      return;
    }

    //save and empty the field
    const valueToSave = this.new_comment_text;
    this.new_comment_text = "";
    const commentToSave = new YopComment(stackUserId, stackUserId, displayedStackId,
                                         checkId, this.authenticated_user_id, valueToSave, valueToSave,
                                         new Date().getTime(), new Date().getTime());

    console.log("Comment to save: ", commentToSave);

    // TODO: validate user id is defined before firing the backend call

    this.commentService.saveAComment(stackUserId, displayedStackId, checkId, commentToSave)
    .subscribe( 
      comment => {
        console.log("comment saved OK.");
        if ( this.comments[checkId] == undefined ) {
          this.comments[checkId] =  [];
        }
        this.stackUiLogicService.setUserNameInComments([[comment]]);
        comment.text_formatted = YopMarked.getInstance().render(comment.text);
        this.comments[checkId].push(comment);
        this.comments[checkId].sort((a,b)=> {return b.timestamp - a.timestamp;});
      }, 
      () => { 
        console.log("Failed to save comment");
      }
      );
  }


  public submitCheckFromMissedOrFuture(check: Check) {
    // set the checking active 
    this.checkActive = true;
    this.checkForUpdate = undefined;
    this.checkPeriodDate = new Date(check.date);
  }


  /**
  * post action after a successful check
  */
  public OnCheckCompleted(checkSaved: Check) {
      this.closeChecking();
      if (!checkSaved) { 
        console.log("OnCheckCompleted: no check_saved provided: ",checkSaved);
        return;
      }
      console.log("OnCheckCompleted: check posted: ",  checkSaved);
      this.onStackChecked.emit([checkSaved]);
      //this.checks = this.stackUiLogicService.handleNewCheck(this.checks, checkSaved, this.displayed_stack);
  }

  public OnCheckAborted(data: any) {
      console.log("Check aborted");
      this.closeChecking();
  }

  /**
   * Closing checking
   */
  private closeChecking() {
      this.checkActive  = false;
      this.checkForUpdate = undefined;
  }
}
