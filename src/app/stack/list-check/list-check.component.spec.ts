import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';



import { ListCheckComponent } from './list-check.component';

import { GlobalService, StackType } from '../../services/global.service';
import { StackUiLogicService } from '../../services/stack-ui-logic.service';
import { CheckService } from '../../services/check.service';
import { CommentService } from '../../services/comment.service';

import { YopComment } from '../../services/comment';
import { Check } from '../../services/check';
import { Stack } from '../../services/stack';


describe('ListCheckComponent', () => {
  let component: ListCheckComponent;
  let fixture: ComponentFixture<ListCheckComponent>;

  let mockCommentService;
  let mockCheckService;
  let mockStackUiLogicService;


  beforeEach(async(() => {

    mockCommentService = jasmine.createSpyObj("CommentService", ["saveAComment"]);
    mockCommentService.saveAComment.and.callFake((comment) => { 
      return Observable.create(observer => { observer.next({"text":"some text"});observer.complete(); });
    })

    mockStackUiLogicService = jasmine.createSpyObj("StackUiLogicService", ["setUserNameInComments", "fillGapsInChecks"]);
    
    mockCheckService = jasmine.createSpyObj("CheckService", ["updateACheck"]);
    mockCheckService.updateACheck.and.callFake((check) => { 
      return Observable.create(observer => { observer.next({"text":"some text"});observer.complete(); });
    })


    TestBed.configureTestingModule({
      declarations: [ ListCheckComponent ],
      providers: [
        GlobalService,
        { provide: CommentService, useValue: mockCommentService },
        { provide: CheckService, useValue: mockCheckService },
        { provide: StackUiLogicService, useValue: mockStackUiLogicService },
      ], 
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  const commentId = "commentId";
  const stackUserId = "stackUserId";
  const stackId = "stackId";
  const checkId = "checkId";
  const commentUserId = "commentUserId";
  const text = "initial text";

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.comments[0] = []
    component.comments[0].push(new YopComment(commentId, stackUserId, stackId,
                                         checkId, commentUserId, text, "",
                                         new Date().getTime(), new Date().getTime()));
    console.log("Fixtured component: ", component);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('new comment', () => {
    it('should not be posted if empty string',
      inject([StackUiLogicService, CheckService, CommentService], 
        (stackUiLogicService: StackUiLogicService, checkService: CheckService, commentService: CommentService) => {

          component.new_comment_text = "";
          expect(component.new_comment_text).toEqual("");
          expect(component.new_comment_text == text ).toBeFalsy();
          component.insertCommentToCheck(checkId , stackUserId, stackId);

          expect(commentService.saveAComment).not.toHaveBeenCalled();
        }));

    it('should not be posted if user is undefined',
      inject([StackUiLogicService, CheckService, CommentService], 
        (stackUiLogicService: StackUiLogicService, checkService: CheckService, commentService: CommentService) => {
          component.authenticated_user_id = undefined;
          component.new_comment_text = "something";

          expect(component.authenticated_user_id).toBeUndefined();
          component.insertCommentToCheck(checkId , stackUserId, stackId);

          expect(commentService.saveAComment).not.toHaveBeenCalled();
        }));
  })


  describe('new check', () => {
    describe('for stacks of type TIME', () => {
      it('should trigger the computation of the missed period/checks',
        inject([StackUiLogicService, CheckService], 
          (stackUiLogicService: StackUiLogicService, checkService: CheckService) => {

            component.displayed_stack = {'creation_date' : new Date().getTime(),'type': StackType.TIME} as Stack;
            component.handleNewCheck(new Check("1", new Date().getTime(), "...", "comment"));

            expect(stackUiLogicService.fillGapsInChecks).toHaveBeenCalled();
      }));
    });
    describe('for stacks of type NOTES', () => {
      beforeEach(() => {
          component.displayed_stack = {'creation_date' : new Date().getTime(), 'type': StackType.NOTES} as Stack;
      });
      it('should not trigger the computation of the missed period/checks',
        inject([StackUiLogicService, CheckService], 
          (stackUiLogicService: StackUiLogicService, checkService: CheckService) => {

            component.handleNewCheck(new Check("1", new Date().getTime(), "...", "comment"));

            expect(stackUiLogicService.fillGapsInChecks).not.toHaveBeenCalled();
          }));
      it('should be sorted',
        inject([StackUiLogicService, CheckService], 
          (stackUiLogicService: StackUiLogicService, checkService: CheckService) => {
            const dateRef = new Date().getTime();
            const futureDate = dateRef + 600;
            component.handleNewCheck(new Check("1", dateRef , "...", "comment"));
            component.handleNewCheck(new Check("2", futureDate , "...", "comment"));

            expect(component.checks[1].id).toEqual("1");
          }));
      });
  });

  describe('check update', () => {
    describe('for stacks of type TIME', () => {
      it('should trigger the computation of the missed period/checks',
        inject([StackUiLogicService, CheckService], 
          (stackUiLogicService: StackUiLogicService, checkService: CheckService) => {

            component.displayed_stack = {'creation_date' : new Date().getTime(), 'type': StackType.TIME} as Stack;
            component.check_date_edited =  {'value': 'new text'};

            component.updateCheck(new Check("1", new Date().getTime(), "...", "comment"), "plop");

            expect(stackUiLogicService.fillGapsInChecks).toHaveBeenCalled();
      }));
    });
  });

});
