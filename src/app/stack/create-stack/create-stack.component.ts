import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StackStatus, GlobalService } from '../../services/global.service'


import { Stack }              from '../../services/stack';
import { StackService }       from '../../services/stack.service';
import { User }               from '../../services/user';
import { UserService }        from '../../services/user.service';

@Component({
  selector: 'app-create-stack',
  templateUrl: './create-stack.component.html',
  styleUrls: ['./create-stack.component.css']
})
export class CreateStackComponent implements OnInit {
  
  @Input() user_id : string;
  @Output() onCreationCompleted: EventEmitter<any> = new EventEmitter();

  private stackService : StackService;
  private userService : UserService;
  private globalService : GlobalService;

  private StackStatus = StackStatus;

  public new_stack_name        : string;
  public new_stack_desc        : string;
  public new_stack_periodicity : number;
  public new_stack_frequency   : number;
  public new_stack_type        : number;
  public new_stack_status      : StackStatus;
  public new_stack_shared      : boolean;
  public new_stack_privacy     : number;


  // shared stack specific
  public username_like : string;
  public user_like_result : User[] = [];
  public shared_stack_selected_users: Map<string, string> = new Map();

  // DRAFT ___ //
  public some_description = ['A Time contest stack helps you keeping your motivation for recurent activities',
                             'A Notes stack is a simple way to keep some notes online and share them with friends',
                             'Get stuff done with a KANBAN stack (experimental)...'];

  private page_sequence = { '0': ['Name selection', 'Type selection', 'Time settings', 'Privacy selection'],
                            '1': ['Name selection', 'Type selection',                  'Privacy selection'],
                            '3': ['Name selection', 'Type selection',                  'Privacy selection']}

  public wizard_current_step : string;
  public wizard_next : string;
  public wizard_previous : string;
  public wizard_first : string;
  public wizard_last : string;

  // ---------------
  

  // public for component template access
  public show_stack_creation_form : boolean = false;


  constructor(stackService: StackService, userService: UserService, globalService: GlobalService) { 
    this.stackService  = stackService;
    this.userService   = userService;
    this.globalService = globalService;
  }

  ngOnInit() {
    this.init_wizard();
  }

  public init_wizard() {
    this.new_stack_name        = "";
    this.new_stack_desc        = "";
    this.new_stack_periodicity = 2;
    this.new_stack_frequency   = 1;
    this.new_stack_type        = 0;
    this.new_stack_status      = StackStatus.RUNNING;
    //this.new_stack_shared      = false;
    this.new_stack_privacy     = 0;

    this.wizard_current_step = this.page_sequence[this.new_stack_type][0];
    this.wizard_next         = this.page_sequence[this.new_stack_type][1];
    this.wizard_previous     = this.wizard_current_step;
    this.wizard_first        = this.wizard_current_step;
    this.wizard_last         = this.page_sequence[this.new_stack_type][this.page_sequence[this.new_stack_type].length - 1];
  }

  public post_stack() {
      console.log("post_stack: " + this.new_stack_name);
        if ( this.new_stack_name !== '') {
          console.log("Creating stack, with name: " + this.new_stack_name + " for user: " + this.user_id);

          let stack_to_be_created = new Stack( 
                this.user_id, this.new_stack_name, this.new_stack_desc, 
                this.new_stack_periodicity,0,0, 
                this.new_stack_frequency, new Date().getTime(),
                Number(this.new_stack_type), this.new_stack_privacy == 0, this.new_stack_status, 
                this.new_stack_privacy == 2,
                [this.user_id],
                Array.from(this.shared_stack_selected_users.keys()), 
                1234567);

          this.stackService.createStack(this.user_id, stack_to_be_created)
            .subscribe( 
            stack => 
            {
              console.log("Result: " + stack  );
              //TODO: find a cleaner way to clean the form
              this.new_stack_name = "";
              this.new_stack_desc = "";
              this.show_stack_creation_form = false;
              this.init_wizard();
              console.log("emitting !");
              this.onCreationCompleted.emit([stack]);
            }, 
            () => {
              console.log("Creation failed");
              this.onCreationCompleted.emit([null]);
            });
        }
   } 
   
  public wizard_move_forward() {
    console.log("wizard move forward: ", this.new_stack_type, this.page_sequence);
    let wiz_seq : string[]  = this.page_sequence[this.new_stack_type];
    let current_step_index  = wiz_seq.indexOf(this.wizard_current_step);
    let last_index          = wiz_seq.length - 1;
    let next_step_index     = current_step_index;
    let previous_step_index = current_step_index;


    this.wizard_current_step = wiz_seq[Math.min(last_index, current_step_index+1)]
    this.wizard_previous     = wiz_seq[current_step_index]
    this.wizard_next         = wiz_seq[Math.min(last_index, current_step_index+2)]
    //console.log("forward: current: ", this.wizard_current_step, " previous: ", this.wizard_previous, " next: ", this.wizard_next);
  }

  public wizard_move_back() {
    let wiz_seq : string[]  = this.page_sequence[this.new_stack_type];
    let current_step_index  = wiz_seq.indexOf(this.wizard_current_step);
    let last_index          = wiz_seq.length - 1;
    let next_step_index     = current_step_index;
    let previous_step_index = current_step_index;


    this.wizard_current_step = wiz_seq[Math.max(0, current_step_index-1)]
    this.wizard_previous     = wiz_seq[Math.max(0, current_step_index-2)]
    this.wizard_next         = wiz_seq[current_step_index]

    //console.log("back: current: ", this.wizard_current_step, " previous: ", this.wizard_previous, " next: ", this.wizard_next);
  }


} //end



