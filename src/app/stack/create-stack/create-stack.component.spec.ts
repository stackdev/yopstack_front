import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CreateStackComponent } from './create-stack.component';

import { GlobalService } from '../../services/global.service';
import { UserService } from '../../services/user.service';
import { StackService } from '../../services/stack.service';


describe('CreateStackComponent', () => {
  let component: CreateStackComponent;
  let fixture: ComponentFixture<CreateStackComponent>;

  let mockStackService; 
  let mockUserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateStackComponent ],
      providers: [
        GlobalService,
        { provide: UserService, useValue: mockUserService },
        { provide: StackService, useValue: mockStackService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
