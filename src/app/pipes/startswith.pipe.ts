import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'startswith'
})
export class StartswithPipe implements PipeTransform {
  transform(fullText: string, textMatch: string): boolean {
    return fullText.startsWith(textMatch);
  }
}