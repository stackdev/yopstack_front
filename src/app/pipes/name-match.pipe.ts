import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameMatch'
})
export class NameMatchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    //console.log("pipe", value);
    if(value !== undefined) {
      return value.filter( v => v.match);
    }
  }

}
