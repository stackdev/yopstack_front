import { Component, OnInit } from '@angular/core';
import { BackendInfoService } from '../services/backend-info.service';
import { AuthService } from '../services/auth.service';

declare function require(moduleName: string): any;
const {changelog, latest_version} = require('../../../changelog.json');
import * as marked  from 'marked';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
  })
export class AboutComponent implements OnInit {

  public front_changelog: any;
  public front_latest_version : any;
  public back_changelog: any = [{"version": "", "date": "", "log": "changelog"}];
  public back_latest_version : any = "";
  public backChangelog : any[] = [];

  constructor(public backendInfoService: BackendInfoService, public authService: AuthService) {

    this.front_latest_version = latest_version;
    this.front_changelog = changelog;
    this.front_changelog.forEach((p, i, arr) => arr[i].formatted = marked(arr[i].log));

  }

  ngOnInit() {
    this.authService.loggedIn("")
    .subscribe((logged_user) => {console.log("OK authenticated, can continue")});


    this.backendInfoService.getBackendInfo()
    .subscribe(
      (info) => {
        console.log("Backend version retrieved !: ", info);
        this.back_latest_version = info.application.version.replace(/_/g, ".");
        Object.keys(info.application.changelog)
          .forEach(change => {
            let log = info.application.changelog[change];
            log.formatted = marked(log.log.replace(/|/, "\n"));
            log.version = change.replace(/_/g,".");
            this.backChangelog.push(log);
          });
          console.log(this.backChangelog);
      },
      (err) => console.log("backendinfo error..."));


  }

}
