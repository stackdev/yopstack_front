import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import {Observable} from 'rxjs/Observable'

import { AboutComponent } from './about.component';
import { AuthService } from '../services/auth.service';
import { BackendInfoService } from '../services/backend-info.service';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;
  let mockBackendInfoService: any;
  let mockAuthService: any;

  beforeEach(async(() => {
    mockBackendInfoService = jasmine.createSpyObj('BackendInfoService', ['getBackendInfo']);
    mockBackendInfoService.getBackendInfo.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});

    mockAuthService = jasmine.createSpyObj('AuthService', ['loggedIn']);
    mockAuthService.loggedIn.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});


    TestBed.configureTestingModule({
      declarations: [ AboutComponent ], 
      providers: [
      {provide: BackendInfoService, useValue: mockBackendInfoService},
      {provide: AuthService, useValue: mockAuthService}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});