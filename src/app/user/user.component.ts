import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User }        from '../services/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private auth: AuthService;
  
  private userService: UserService;

  // public for component template access
  public identified_user: User;
  public error_message: string;
  public inputName: string = "";


  constructor(userService: UserService, auth: AuthService) {
    this.auth = auth;
    this.userService = userService;
  }

  ngOnInit() {
    this.auth.loggedIn("");
  }


  public search_user(user_searched: string) {
      if ( user_searched !== '') {
        console.log("Searching username: " + user_searched);
        //console.log("http auth used:" + $http.defaults.headers.common.Authorization );

        this.userService.getAUserByName(user_searched).subscribe(
        user => {
          console.log("Result: " + user.name  );
          this.identified_user  = user;
          this.error_message = "";
        }, 
        () => {
          this.error_message = "No user found by name: '" + user_searched + "'... try again with another name.";
          });
      } else {
        console.log("No user id provided");
      }};

   public toggleSearchUser() {
      this.identified_user = null;
   }

} // end 



/*



    $scope.sid = $routeParams.uid;

    $scope.inputName = '';
    $scope.identified_user = '';
    $scope.resultUser = {};

    $scope.search_user = function(user_searched) {
      if ( user_searched !== 'undefined' 
        && user_searched !== '') {
        console.log("Searching username: " + user_searched);
        console.log("http auth used:" + $http.defaults.headers.common.Authorization );

        UserResource.getByName(user_searched, function(user) {
          console.log("Result: " + user.name  );
          $scope.identified_user  = user;
        });
      } else {
        console.log("No user id provided");
      }};

      $scope.toggleSearchUser = function() {
        $scope.identified_user = '';
      }
*/

/*    $scope.check = function() {
      if ( typeof $scope.inputId !== 'undefined' && $scope.inputId !== '') {
        console.log("Checking user, with id: " + $scope.inputId);
        //testing userResource service
        UserResource.getById($scope.inputId, function(user) {
          // each item is an instance of CreditCard
          //expect(user instanceof User).toEqual(true);
          console.log("Result: " + user.name  );
          $scope.resultUser = user;
          $scope.inputId = "";
        });
      } else if ( typeof $scope.inputName !== 'undefined' && $scope.inputName !== '') {
        console.log("Checking user, with name: " + $scope.inputName);
        console.log("http auth used:" + $http.defaults.headers.common.Authorization );

        UserResource.getByName($scope.inputName, function(user) {
          console.log("Result: " + user.name  );
          $scope.resultUser = user;
          $scope.inputName = "";
        });
      } else {
        console.log("No user id provided");
      }};

    $scope.checkOrCreateUser = function() {
      console.log($scope.requestedUser);;
    };

    $scope.create = function() {
      if ( typeof $scope.inputName !== 'undefined' && $scope.inputName !== '') {
        console.log("Creating user, with name: " + $scope.inputName);
        console.log("http auth used:" + $http.defaults.headers.common.Authorization );
        UserResource.resource.save( {id: 'f1ff5eca-f831-4884-adcd-c7c9a9135b23', name: $scope.inputName }, function(user) {
          //expect(user instanceof User).toEqual(true);
          console.log("Result: " + user.name  );
          $scope.resultUser = user;
          $scope.inputName = "";
        });

      } else {
        console.log("No user name provided");
      }
    }*/

  //}

