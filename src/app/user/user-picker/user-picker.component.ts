import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../services/user';

@Component({
  selector: 'app-user-picker',
  templateUrl: './user-picker.component.html',
  styleUrls: ['./user-picker.component.css']
})
export class UserPickerComponent implements OnInit {

  public usernameLike: string = "";
  
  public userLikeResult: Map<string, User> = new Map();
  public userService: UserService;

  @Input() usersToAdd: Map<string, User> = new Map();
  @Input() currentUserId: string;
  @Output() onSelectionChanged: EventEmitter<any> = new EventEmitter();

  constructor(userService: UserService) {
    this.userService = userService;
   }

  ngOnInit() {
    console.log("user picker init: input usersToAdd: ", this.usersToAdd[0]);
  }


  /**
  * [searchUsernameLike description] 
  * User search for addition to shared stack
  */
 public searchUsernameLike() {

  if (this.usernameLike == undefined || this.usernameLike == "") {
    console.log("Not searching empty user.");
    return;
  }

  this.userService.getListUserByNameLike(this.usernameLike)
    .subscribe(
      user_list => {
        console.log("success: ", user_list);
        let to_remove_list : string[] = [];

        //remove current user from result
        let index = user_list.map(v=>v.id)
                   .indexOf(this.currentUserId);
        if (index > -1) {
            user_list.splice(index, 1);
        }
        //remove already selected users from result
        //this.userLikeResult = new Map(user_list.filter(u => !this.usersToAdd.has(u.id)).map(u => [u.id, u]));
        this.userLikeResult = user_list.filter(u => !this.usersToAdd.has(u.id)).reduce((map, u) => map.set(u.id, u), new Map());
      },
      e => {
        console.log("Error returned while retrieving user list matching: ", this.usernameLike);
      }
    );
  }

  /**
   * @param {String} user to be removed from selected list
   */
  public removefromSelectedUsers(userId: string) {

    if(this.usersToAdd.has(userId)) {
      this.userLikeResult.set(userId, this.usersToAdd.get(userId));
      this.usersToAdd.delete(userId);

      // TODO: check if a better way to bind usersToAdd to parent directly
      this.onSelectionChanged.emit(this.usersToAdd);
    }
  }

  /**
   * @param user 
   */
  public addToSelectedUsers (userId: string, userName: string) {
    console.log("Adding user ?", userId);

    if(!this.usersToAdd.has(userId)) {
      this.usersToAdd.set(userId, this.userLikeResult.get(userId));
      
      // TODO: check if a better way to bind usersToAdd to parent directly
      this.onSelectionChanged.emit(this.usersToAdd);

    }
    //remove user from list
    let index = this.userLikeResult.delete(userId);
  }

}
