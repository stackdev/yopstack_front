import { async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Http, BaseRequestOptions } from "@angular/http";
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { Observable } from 'rxjs/Observable';

import { MockBackend } from "@angular/http/testing";

import { UserComponent } from './user.component';
import { UserService } from '../services/user.service';
import { GlobalService } from '../services/global.service';
import { CommonService } from '../services/common.service';
import { AuthService } from '../services/auth.service';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  let mockRouter : any;
  let mockCookieService : any;
  let mockAuthService : any;
  let mockUserService;

  beforeEach(async(() => {
    
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);
    mockCookieService = jasmine.createSpyObj('CookieService', ['get', 'remove']);
    mockAuthService = jasmine.createSpyObj('AuthService', ['loggedIn']);
    mockAuthService.loggedIn.and.callFake((user) => { Observable.create(observer => { observer.next({"name":""});observer.complete(); }); });

    mockUserService = jasmine.createSpyObj('UserService', ['getAUserByName']);
    
    TestBed.configureTestingModule({
      declarations: [ UserComponent ],
      providers: [
        {provide: UserService, useValue: mockUserService},
        GlobalService,
        CommonService,
        {provide: AuthService, useValue: mockAuthService},
        MockBackend,
        BaseRequestOptions,
        { provide: Http,
          useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
              return new Http(backendInstance, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        { provide: Router, useValue: mockRouter },
        { provide: CookieService, useValue: mockCookieService},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create',
    inject([UserService], (service: UserService) => {
      expect(component).toBeTruthy();
  }));
});
