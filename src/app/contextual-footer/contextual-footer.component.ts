import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ContextDataService }      from '../services/context-data.service';

@Component({
  selector: 'app-contextual-footer',
  templateUrl: './contextual-footer.component.html',
  styleUrls: ['./contextual-footer.component.css']
})
export class ContextualFooterComponent implements OnInit, OnChanges {


  @Input() public actionOrder: string[];

  public context: string = "";
  public contextDataService: ContextDataService;
  public currentAction: string;

  constructor(contextDataService: ContextDataService) { 
      this.contextDataService = contextDataService;
  }
  
  ngOnInit() {
    console.log("actionOrder: ", this.actionOrder);
    this.contextDataService.currentContext.subscribe(
      new_context => {console.log("New context: " + new_context);
      this.context = new_context;
    }
    );
    
    this.setDefaultAction();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['actionOrder'] && changes['actionOrder'].currentValue != undefined 
    && this.currentAction === undefined) {
      this.setDefaultAction();
    }

  }
  
  public changeAction(newAction: string) {
      this.currentAction = newAction;
      this.contextDataService.changeAction(newAction);
  }

  public setDefaultAction() {
    // setting the default action to the first element  of the action list
    console.log("Contextual footer, setting default action to: ", this.actionOrder[0]);
    this.changeAction(this.actionOrder[0]);
  }

}
