import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { ContextDataService }       from '../services/context-data.service';


import { ContextualFooterComponent } from './contextual-footer.component';

describe('ContextualFooterComponent', () => {
  let component: ContextualFooterComponent;
  let fixture: ComponentFixture<ContextualFooterComponent>;
  let mockContextDataService;

  beforeEach(async(() => {
    mockContextDataService = jasmine.createSpyObj('ContextDataService', ['currentContext']);
    mockContextDataService.currentContext = Observable.create(observer => { observer.next({"name":""});observer.complete(); }); 

    TestBed.configureTestingModule({
      declarations: [ ContextualFooterComponent ],
      providers: [{provide:ContextDataService, useValue:mockContextDataService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
