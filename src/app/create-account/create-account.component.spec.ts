import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CreateAccountComponent } from './create-account.component';
import { UserService } from '../services/user.service';

describe('CreateAccountComponent', () => {
  let component: CreateAccountComponent;
  let fixture: ComponentFixture<CreateAccountComponent>;
  let mockRouter;
  let mockUserService;

  beforeEach(async(() => {
    mockRouter = jasmine.createSpyObj("Router", ["navigate"]);
    mockUserService = jasmine.createSpyObj("UserService", ["checkUserNameAvailibility", "create"]);

    TestBed.configureTestingModule({
      declarations: [ CreateAccountComponent ],
      providers: [
      {provide: Router, useValue: mockRouter },
      {provide: UserService, useValue: mockUserService },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Email check should be always false until it is really implemented...', () => {
    expect(component.isEmailAddressInUse()).toBeFalsy();
  });

});
