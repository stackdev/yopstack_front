import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { AccountCreationCredentials } from '../services/credentials';
import { UserService } from '../services/user.service';
import { BackendInfoService } from '../services/backend-info.service';

import { Observable } from 'rxjs/Observable';
import { User } from '../services/user';



@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  public credentials: AccountCreationCredentials;
  public creation_error: boolean  = false;
  public password2 = "";

  public usernameAvailable: boolean = false;
  public email_valid: boolean = false;
  public password_valid: boolean = false;

  public error_message: string = "";

  public username_minimal_length = 5;

  constructor(public router: Router,
              public userService: UserService,
              private backendInfoService: BackendInfoService) {
    this.credentials = {username:"", password:"", email: ""};
  }

  ngOnInit() { }

    public createAccount() {

    // if (this.isEmailAddressInUse()) {
    //   this.error_message = "The email address is already in use.";
    //   console.log(this.error_message);
    //   this.creation_error = true; 
    //   return;
    // }
    // 
    
    if(!this.checkUsernameLength()) {
      this.error_message = "The username length must be greater than " + this.username_minimal_length + " digits.";
      this.creation_error = true;
      return;
    }

    if (! this.checkPasswordTypedOK()) {
      this.error_message = "The 2 passwords fields are different...";
      console.log(this.error_message);
      this.creation_error = true; 
      return;
    }

    this.isUserAvailable()
    .flatMap((available) => {
      console.log("User service response: ", available);
      if (available === true ) {
        let new_user : User = new User("",this.credentials.username,null, null, true, null, this.credentials.password, null, null);
        return this.userService.create(new_user);
      }
      throw("User already in use");
    })

      .subscribe(
      (ok) => {
        console.log("User creation ok:", ok);
        this.creation_error = false;
        console.log("User creation completed successfully !");
        this.router.navigate(['/login'], { queryParams:{username: this.credentials['username'], newaccount:'1'}});
      },
      (failed)=> { 
        //this.error_message = "The username " + this.credentials.username +" is already taken...";
        console.log("User creation failed... ", failed);
        this.error_message = "User creation failed: ", failed;
        this.creation_error = true; 
        return; 
      }
    );
  }

  public isEmailAddressInUse() : boolean {
    this.email_valid = true;
    return false;
  }

  public checkPasswordTypedOK() : boolean {
    if(this.credentials['password'] !== "" &&
       this.password2 !== "" &&
       this.credentials['password'] === this.password2) {
      this.password_valid = true;
      return true;
    }
    this.password_valid = false;
    return false;
  }

  public isUserAvailable() : Observable<boolean>{
    if(this.credentials['username'] !== "") {
      return this.userService.checkUserNameAvailibility(this.credentials['username']);
    }
    this.usernameAvailable = false;
  }

  public checkUsernameLength() {
    if (this.credentials['username'].length < this.username_minimal_length )
      return false;
    return true;
  }


}
