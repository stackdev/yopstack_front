import { Component, ViewEncapsulation, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute }    from '@angular/router'; 



import { CheckService }      from '../services/check.service';
import { Check }             from '../services/check';

import * as d3 from 'd3';

@Component({
  selector: 'app-stats',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit, OnChanges {

  @Input() public checks: Check[];


  private checkService: CheckService;
  private route:        ActivatedRoute;
  private stack_id:     string;
  private before_date:  string;
  private after_date:   string;
  private sub:          any;




  constructor(route: ActivatedRoute, checkService: CheckService) { 
    this.checkService = checkService;
    this.route = route;
  }

  //TODO: check if logged in
  //TODO: handle before and after date (this info must be propagated from list_check component to stack_component until here)
  ngOnInit() {

    // this.sub = this.route.params
    // .subscribe(
    //   params => {
    //     this.stack_id    = params['sid'];
    //     // this.before_date = params['before_date'];
    //     // this.after_date  = params['after_date'];
    //     console.log("stack_id: ", this.stack_id);
  
    //     this.before_date=(new Date()).toString();
    //     this.after_date="0";


    //     this.checkService.getChecksBetweenDates(this.stack_id, this.before_date, this.after_date)
    //     .subscribe(
    //       checks => {
    //         console.log("Result: " + checks[0].id);
    //         this.checks = checks;
    //         this.render_checks_viz(checks);
    //       },
    //       () => {
    //         console.log("No Checks found");
    //         this.checks = [];
    //       });
    //     });
  }

  public ngOnChanges(changes: SimpleChanges) {
    if(changes['checks'] && changes['checks'].currentValue != undefined ) {
      this.render_checks_viz(this.checks);
    }
  }

  public render_checks_viz(checks: any[]) {
    var svg = d3.select('svg');
    var width = svg.node().getBoundingClientRect().width;
    var height = svg.node().getBoundingClientRect().height;
    
    console.log("processing: " + checks);
    //console.log("svg width: " + svg_width + " height: " + svg_height);
    // Set the dimensions of the canvas / graph
    // var margin = {
    //   top:    30,
    //   right:  20,
    //   bottom: 100,
    //   left:   50
    // },
    var margin = {
      top:    30,
      right:  10,
      bottom: 30,
      left:   10
    }
    // ,
    // width  = svg_width - margin.left - margin.right,
    // height = svg_height - margin.top  - margin.bottom;

    var dataSet = checks;

    //order dataSet to print the largest circle first
    dataSet = dataSet.sort(function(a, b){
      return b.date - a.date;
    });

    var minDataPoint = d3.min(dataSet, function(d) {return +d.date});
    var maxDataPoint = d3.max(dataSet, function(d) {return +d.date});
    console.log("min/max data: ", minDataPoint , ", ", maxDataPoint);

    var timeScale = d3.scaleTime()
    .domain([minDataPoint,maxDataPoint])
    .range([0, width]);


    // general time formating: 
    var shortTimeFormat = d3.timeFormat("%a %e %b");

    console.log("Processing dataset: ", dataSet);

    svg
       //responsive SVG needs these 2 attributes and no width and height attr
       .attr("preserveAspectRatio", "xMinYMin meet")
       .attr("viewBox", "0 0 " + width + " " + height)
       //class to make it responsive
       .classed("svg-content-responsive", true); 

    console.log("Selected svg: ", svg);

    // Define the div for the tooltip
    var tooltip = d3.select(".tooltip");
    if (tooltip.empty()) {
        tooltip = d3.select("html").append("div")
    }
    tooltip.attr("class", "tooltip")       
           .style("opacity", 0);


    svg.selectAll('circle')
    .data(dataSet)
    .enter()
    .append('circle')
    .attr('r',      (d, i) => { return d.comment.length; })
    .attr('cx',     (d, i) => { return timeScale(d.date);  })
    .attr('cy',     (d, i) => { return height/2;  })
    .attr('class',  (d)    => { return "mycircle"; })
    .on("mouseover", function(d) {    
          tooltip.transition()    
              .duration(200)    
              .style("opacity", .9);    
          tooltip.html(shortTimeFormat(d.date) + ": </br>  "  + d.comment) 
              .style("left", (d3.event.x) + "px")   
              .style("top", (d3.event.y - 28) + "px");  
          })          
      .on("mouseout", function(d) {   
          tooltip.transition()    
              .duration(500)    
              .style("opacity", 0); 
      });

  var xAxis = d3.axisBottom()
      .scale(timeScale)
      .ticks(3)
      .tickFormat(shortTimeFormat);

  var xAxisGroup = svg.append("g")
   .attr("class", "xaxis")   // give it a class so it can be used to select only xaxis labels  below
   .attr("transform", "translate(0," + (height - margin.bottom) + ")")
   .call(xAxis);

   svg.append("text")
      .attr("x", (width / 2))             
      .attr("y", 0 + (margin.top / 2))
      .attr("text-anchor", "middle")  
      .style("font-size", "16px")  
      .text("Check comment size over time");


    console.log("rendering done... ", d3, svg);

  }
} // 
