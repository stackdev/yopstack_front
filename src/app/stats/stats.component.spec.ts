import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Observable } from 'rxjs/Observable';


import { ActivatedRoute } from '@angular/router';
import { CheckService } from '../services/check.service';
import { StatsComponent } from './stats.component';

describe('StatsComponent', () => {
  let component: StatsComponent;
  let fixture: ComponentFixture<StatsComponent>;

  let mockActivatedRoute : any;
  let mockCheckService : any;


  beforeEach(async(() => {
    mockActivatedRoute = jasmine.createSpyObj('ActivatedRoute', ['navigate']);
    mockCheckService = jasmine.createSpyObj('CheckService', ['getChecksBetweenDates']);
    mockActivatedRoute.params = Observable.create(observer => { observer.next({"name":""});observer.complete(); });

    mockCheckService.getChecksBetweenDates.and.callFake((stackId: string, before_date: string, after_date: string) => { Observable.create(observer => { observer.next({"name":""});observer.complete(); }); });

    

    TestBed.configureTestingModule({
      declarations: [ StatsComponent ],
      providers: [{ provide: ActivatedRoute, useValue: mockActivatedRoute },
                  { provide: CheckService, useValue: mockCheckService},
                 ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(StatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
