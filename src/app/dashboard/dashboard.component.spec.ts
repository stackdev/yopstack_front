import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { GlobalService } from '../services/global.service';
import { StackService } from '../services/stack.service';
import { PreferenceService } from '../services/preference.service';
import { StackUiLogicService } from '../services/stack-ui-logic.service';
import { CommonService } from '../services/common.service';
import { ContextDataService } from '../services/context-data.service';
import { NameMatchPipe } from '../pipes/name-match.pipe';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  
  let mockRouter;

  // our services
  let mockAuthService;
  let mockUserService;
  let mockGlobalService;
  let mockStackService;
  let mockPreferenceService;
  let mockStackUiLogicService;
  let mockCommonService;
  let mockContextDataService;

  beforeEach(async(() => {
    mockAuthService = jasmine.createSpyObj('AuthService', ['loggedIn']);
    mockAuthService.loggedIn.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});


    TestBed.configureTestingModule({
      declarations: [ DashboardComponent, 
                      NameMatchPipe
                    ],
      providers: [
        { provide: AuthService, useValue: mockAuthService },
        { provide: UserService, useValue: mockUserService },
        GlobalService,
        { provide: StackService, useValue: mockStackService },
        { provide: PreferenceService, useValue: mockPreferenceService },
        { provide: Router, useValue: mockRouter },
        { provide: StackUiLogicService, useValue: mockStackUiLogicService },
        { provide: CommonService, useValue: mockCommonService },
        { provide: ContextDataService, useValue: mockContextDataService },
        
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
