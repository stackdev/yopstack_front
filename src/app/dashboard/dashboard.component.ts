import { Component, OnInit, Input, OnChanges, SimpleChanges , OnDestroy}  from '@angular/core';
import { Router }                                              from '@angular/router';

import { Observable } from 'rxjs/Observable';


import { AuthService }             from '../services/auth.service';
import { UserService }             from '../services/user.service';
import { StackService }            from '../services/stack.service';
import { PreferenceService }       from '../services/preference.service';
import { GlobalService, StackType, StackSorting, StackStatus} from '../services/global.service';
import { CommonService }           from '../services/common.service';
import { StackUiLogicService }     from '../services/stack-ui-logic.service';
import { ContextDataService }      from '../services/context-data.service';


import { Stack }                   from '../services/stack';
import { User }                    from '../services/user';
import { Preference }              from '../services/preference';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
  })
export class DashboardComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  public displayed_user      : User;
  
  private stacks: Stack[] = [];
  public sorted_stacks: Stack[];
  public is_owner   = false;

  // enum 'types'
  public stackType    = StackType;
  public stackSorting = StackSorting;
  public stackStatus  = StackStatus;

  // public for component template access
  public is_loading = false;

  // stack sorting
  public sort_options = ['creation date', 'type', 'last check date'];

  public selected_sort   : StackSorting;
  public selected_status : StackStatus;
  // TMP
  public default_status_filter: StackStatus;


  // custom option list experiment 
  public show_list:   boolean = false;
  public rising_sort: boolean = true;

  // search input properties
  public searched: string = "";

  constructor(private authService: AuthService, private userService: UserService, public global: GlobalService, 
              private stackService: StackService, private preferenceService: PreferenceService, private router: Router, 
              private stackUiLogicService: StackUiLogicService, private commonService: CommonService,
              private contextDataService: ContextDataService) {
    this.default_status_filter = this.stackStatus.RUNNING;
  }

  
  ngOnDestroy () {
    let authenticatedUserName: string;
    let authenticatedUserId: string;
    if (this.global.authenticatedUser !== undefined ) {
      authenticatedUserName = this.global.authenticatedUser.name;
      authenticatedUserId   = this.global.authenticatedUser.id;
    }
  
    console.log("in destroy: ", this.global.userPreference);
    //save the current preference when the user navigate away from the dashboard
    if(this.global.userPreference &&  this.global.userPreference.stacksorting == this.selected_sort &&
       this.global.userPreference.risingsort == this.rising_sort)
     {
       console.log("Sorting option unchanged, no need to update preferences.")
       return;
     }
     console.log("Global user : ", authenticatedUserName, " id: ", authenticatedUserId );
     if ( this.global.userPreference && authenticatedUserId) {

      let pref = new Preference(this.global.userPreference.id, 
                                authenticatedUserId,
                                this.selected_sort, this.rising_sort, 
                                [], [], [], []);
      this.preferenceService.updateAPreference(pref)
       .subscribe(
        (pref) => {
          console.log("Pref saved");
          this.global.userPreference = pref;
        },
        (err) => {console.log("Pref not saved");}
        );
   }
  }

  ngOnInit() {
    this.authService.loggedIn("")
    .subscribe((logged_user)=>
      {
        console.log("Login check completed, user is: ", logged_user);

        this.contextDataService.changeContext("dashboard_context");
        let searched_username : string = logged_user.name;
        if (this.displayed_user !== undefined && 
            this.displayed_user.name !== undefined &&
            this.displayed_user.name !== "" ) {
          console.log("Display user is provided: ", this.displayed_user);
          searched_username = this.displayed_user.name;

        }
        console.log("Getting user details for username: ", searched_username );

        this.is_loading = true;
        let userObservable = this.userService.getAUserByName(searched_username);
        this.initDisplayUser(userObservable);
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("----",changes.displayed_user);
    if ( changes.displayed_user ) {
      let self = this;
      let userObservable = Observable.create(observer => { observer.next(self.displayed_user);observer.complete();});
      this.initDisplayUser(userObservable);
    } 
  } 

public initDisplayUser (userObservable: Observable<User>) { 
  let authenticatedUserName: string;
  if (this.global.authenticatedUser !== undefined ) {
    authenticatedUserName = this.global.authenticatedUser.name;
  }

  userObservable
  .concatMap(user => {
    console.log("concat map user: ", user);
    this.global.identified_user = user;
    this.displayed_user = user;
    if (this.displayed_user.name === authenticatedUserName) { 
      this.is_owner = true;
    }

    if (this.is_owner && !this.global.userPreference) {
      // TMP to be moved... 
      this.preferenceService.getPreference(user.id, "")
      .subscribe(
        pref => { 
          console.log(`getting some preference: ${pref}`);
          console.log("received pref: ",pref.stacksorting, ", transformed to: ", this.stackSorting[pref.stacksorting]);
          this.global.userPreference = pref;
          this.selected_sort = pref.stacksorting;
          this.rising_sort = pref.risingsort;
          this.sort_and_filter_stacks();
        },
        err => { 
         console.log(`Error while getting preference for user ${user.id}, try to save a new one.`);
         
         if (!user.id) {
           console.log("Not saving, user is not defined.");
           return;
         }

         let pref = new Preference(null, user.id, this.stackSorting.CREATION, this.rising_sort,
                                   [], [], [], []);
         this.preferenceService.saveAPreference(pref)
         .subscribe(
          (pref) => {
            console.log("Pref saved");
            this.global.userPreference = pref;
            this.sort_and_filter_stacks();
          },
          (err) => {
            console.log("Pref not saved");
            this.global.userPreference = undefined;
          }
          );

        }
      );
    } else {
      this.rising_sort = this.global.userPreference.risingsort;
      this.selected_sort = this.global.userPreference.stacksorting;
    }
  return this.stackService.getAllStacksByUserid(user.id);
  })
    .subscribe(
      stacks => { 
        this.stacks = stacks;

        stacks.map(stack =>
            {
              stack.is_late = !this.stackUiLogicService
              .computeNextCheckDate(stack.frequency, stack.periodicity, new Date(stack.last_check), new Date(stack.creation_date))
              .current_period_checked;
              stack.match = true;
            });
        this.sort_and_filter_stacks();
        this.is_loading = false;

        },
        () => {console.log("error..."); 
        this.is_loading = false;
        //this.is_owner   = false;
      }
      );
  }

  public onSelect(user: User, stack: Stack) {
    this.router.navigate(["/users", user.id, "stacks", stack.id] );
  }

  public change_sorting(direction : string) {
    this.selected_sort = this.stackSorting[direction];
    this.sort_and_filter_stacks();
  }

  public change_status(new_status: string) {
    this.selected_status = this.stackStatus[new_status];
    this.sort_and_filter_stacks();
  }

  public on_creation_completed(created_stack: Stack) {
    console.log("on_creation_completed: ", created_stack);
    if (created_stack != undefined && created_stack.id != undefined ) {
      this.stacks.push(created_stack);
      this.sort_and_filter_stacks();
    } else{
      console.log(created_stack);
    }

  }

  public sort_and_filter_stacks() {
    console.log("sorting and filtering stacks...");
    this.sort_stacks();
    this.filter_stacks();
    this.match_stack_name(this.searched);
  }

  public sort_stacks() {

    if (this.selected_sort == this.stackSorting.CREATION) {
      this.sorted_stacks = this.stacks.sort(function(a, b) { 
        return a.creation_date - b.creation_date;})
    }else if (this.selected_sort == this.stackSorting.LAST_CHECK) {
      this.sorted_stacks = this.stacks.sort(function(a, b) { 
        return a.last_check - b.last_check;});
    } else if (this.selected_sort == this.stackSorting.TYPE) {
      let grouped_stacks = this.commonService.groupBy(this.stacks, 'type');
      console.log("sorting by type result: ", grouped_stacks);
      let sorted = [];
      for (let stack_type in this.global.keys(this.stackSorting)) {
        if (grouped_stacks[stack_type] ) {
          sorted = sorted.concat(grouped_stacks[stack_type]);
        }
      }
      this.sorted_stacks = sorted;
      console.log("sorting by type result: ", this.sorted_stacks);
    } else {
      this.sorted_stacks = this.stacks;
    }
    if(this.rising_sort == true) {
        this.sorted_stacks.reverse();
    }
  }

  public filter_stacks() {
    //then filter
    let status_filter = this.selected_status || this.stackStatus.RUNNING;
    this.sorted_stacks = this.sorted_stacks.filter(stack => stack.status === undefined || stack.status == status_filter);
  }

  public match_stack_name(search_input: string) {
      search_input = search_input.toLowerCase();
      console.log("Searching: ", search_input);
      this.sorted_stacks = this.sorted_stacks.map(x => {
          return Object.assign({}, x, {
              match: x.name.toLowerCase().includes(search_input)
          });
      }); 
      console.log(this.sorted_stacks);
  }

  // // Custom option list experiment to be moved to a component
  // public show_option_list() {
  //   this.show_list = ! this.show_list;
  //   console.log("toggling option list, ", this.show_list);
  // }

  // public option_selected(option_selected) {
  //   this.show_list = false;
  //   console.log("option selected, ", this.show_list, " option_selected: ", option_selected);
  //   this.selected_sort = option_selected;
  //   this.sort_and_filter_stacks();
  // }


  public change_sort_direction() {
    this.rising_sort = ! this.rising_sort;
    this.sort_and_filter_stacks();
  }
  // end 

}
