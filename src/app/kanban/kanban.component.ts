import { Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

import { GlobalService } from '../services/global.service';
import { CommonService } from '../services/common.service';
import { CheckService }      from '../services/check.service';
import { Check }             from '../services/check';
import { Stack }             from '../services/stack';
import { User } from '../services/user';

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.css']
})
export class KanbanComponent implements OnInit, OnChanges{


  @Input() public checks: Check[];
  @Input() public displayedStack: Stack;
  @Input() public checkers: Map<string, User>;
  @Output() onCheckUpdated: EventEmitter<any> = new EventEmitter();

  public checks_id_map: any = {}

  public mappings = {"PLANNED":  "todo",
                     "DOING":    "doing",
                     "FAILED":    "done",
                     "OK":       "done",}

  public columnMappings = {"todo":  ['doing'],
                           "doing": ['todo', 'done'],
                           "done":  ['doing']}

  public reverse_status_mappings = this.common.reverseObject(this.mappings);

  public kanban_data;

  public checkActive : boolean = false;
  public editedCheck: Check;
  public checkPeriodDate : string;

  constructor(public global: GlobalService, public common: CommonService, public checkService: CheckService) { }
  
  
  ngOnInit() {
    this.refresh_kanban_data(this.checks);
    console.log("[kanban] incoming checks: ", this.checks);
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    console.log("[kanban] change: ", changes, this.kanban_data);

    if(changes['checks']) {
      console.log("[kanban] checks.changes.currentValues: ", changes['checks'].currentValue, 
      " kanban_data: ", this.kanban_data);
      if( changes['checks'].currentValue != undefined && 
          changes['checks'].currentValue.length != 0 ) 
           //&& this.kanban_data === undefined) 
           {
        this.refresh_kanban_data(this.checks);
      }
    }
  }

  public mappingTitleToStatus(cardTitle: string) : string {
    console.log("plop mappingTitleToStatus");
    for (let mapping in this.mappings) {
      if (cardTitle.startsWith(mapping)) {
        return this.mappings[mapping];
      }
    }
    return "none";
  }

  public drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      if (this.handle_priority_change(event.currentIndex, event.container.id)) {
        this.update_and_save_check(event.container.data[event.currentIndex], event.container.id);
      }

    } else {

      console.log("CdkDrag data ?",event, event.previousContainer.data, event.container.data);
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
      this.update_and_save_check(event.container.data[event.currentIndex], event.container.id);
    }
  }
  
  public handle_priority_change(checkIdIndex: number, column: string) : boolean {
    let checksCount = this.kanban_data[column].length
    let checkToBeUpdated = this.checks_id_map[this.kanban_data[column][checkIdIndex]];
    // the list is sorted by date descending (newer first)
    let nextCheck = checkIdIndex > 0 ? this.checks_id_map[this.kanban_data[column][checkIdIndex - 1]]: undefined;
    let previousCheck = checksCount > checkIdIndex - 1 ? this.checks_id_map[this.kanban_data[column][checkIdIndex + 1]]: undefined;

    console.log("handle priority change: previous check: ", previousCheck, ", next check: ", nextCheck);

    if (nextCheck === undefined) {
      if (previousCheck === undefined) {
        return false
      }
      checkToBeUpdated.date = previousCheck.date + 1000;
      return true;
    }

    if (previousCheck === undefined) {
      checkToBeUpdated.date = nextCheck.date - 1000;
      return true;
    }

    // easy case previous and next check don't have the same date
    console.log("Updating check date by adding: ", nextCheck.date - previousCheck.date, nextCheck.date ," - ", previousCheck.date)
    checkToBeUpdated.date = previousCheck.date + (nextCheck.date - previousCheck.date );
    
    return true
    // TODO: harder case previous and next are at the same date we need to move one of them too...
  }


  public update_and_save_check(check_id: string, new_state:string ) {
    let check_to_be_edited: Check = this.checks_id_map[check_id];
    let edited_check: Check = new Check(check_id, check_to_be_edited.date, check_to_be_edited.stack,
      check_to_be_edited.comment, this.reverse_status_mappings[new_state], check_to_be_edited.checker);
    console.log("Emitting updated Check: ", edited_check);
    let obs = this.checkService.updateACheck(edited_check);

    obs.subscribe( 
      checkEdited => {
        this.onCheckUpdated.emit([checkEdited]);
      },
      (error) => {
          console.log("edition failed, restoring check text...", error);
      });
  }

  public refresh_kanban_data(checks: Check[]) {
    console.log("[kanban] refreshing kanban data: ", this.checks, this.kanban_data);
    
    this.kanban_data = {"todo": [], "doing": [], "done": [], "none": []};
    
    if (checks.length == 0) {
      return;
    }
  
    checks.forEach((check) => {
      if (!check.isFrontOnly) {
        let status = this.mappingTitleToStatus(check.status);
        if( this.kanban_data[status] === undefined)
          this.kanban_data[status] = [];
        this.kanban_data[status].push(check.id);
        this.checks_id_map[check.id] = check;
      }
    }); 
    console.log("Kanban data end of refresh: ", this.kanban_data);
    this.sortKanbanColumnByDate()
  }

  public sortKanbanColumnByDate() {
    console.log("sorting kanban_data by date: ", this.kanban_data);
    for(let column in this.kanban_data) {
      this.kanban_data[column].sort((a, b) => {
        return this.checks_id_map[b].date - this.checks_id_map[a].date
      });
    }

  }

  public editKabanTicket(checkId: string) {
    this.checkActive = true;
    if (this.checks_id_map[checkId]) {
      this.editedCheck = this.checks_id_map[checkId];
      console.log("Check edition activated, for check: ", this.editedCheck);
    }
  }

  public OnCheckCompleted(event: any) {
    this.checkActive = false;
  }

  public OnCheckAborted(event: any) {
    this.checkActive = false;
  }

 }

