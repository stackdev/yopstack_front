import { BrowserModule }            from '@angular/platform-browser';
import {BrowserAnimationsModule}    from '@angular/platform-browser/animations';

import { NgModule }                 from '@angular/core';
import { FormsModule, 
         ReactiveFormsModule }      from '@angular/forms';
import { HttpModule, JsonpModule }  from '@angular/http';
import { Observable }               from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { RouterModule, Routes }     from '@angular/router';
//import { MdDatepickerModule, MdNativeDateModule, MaterialModule }       from '@angular/material'; 
import {MatDatepickerModule, MatNativeDateModule, MatInputModule}  from '@angular/material';
import {DragDropModule}             from '@angular/cdk/drag-drop';
import { MatFormFieldModule }       from '@angular/material/form-field';
import { MatSelectModule }          from '@angular/material/select';


// import { CookieService }            from 'angular2-cookie/services/cookies.service';
import { CookieModule }             from 'ngx-cookie';
import { AppComponent }             from './app.component';
import { NavbarComponent }          from './navbar/navbar.component';
import { LoginComponent }           from './login/login.component';
import { UserComponent }            from './user/user.component';
import { LogoutComponent }          from './logout/logout.component';


import { UserService }              from './services/user.service';
import { StackService }             from './services/stack.service';
import { CheckService }             from './services/check.service';
import { CommentService }           from './services/comment.service';
import { PreferenceService }        from './services/preference.service';
import { CommonService }            from './services/common.service';
import { GlobalService }            from './services/global.service';
import { AuthService }              from './services/auth.service';
import { StackUiLogicService }      from './services/stack-ui-logic.service';
import { ContextDataService }       from './services/context-data.service';
import { BackendInfoService }       from './services/backend-info.service';


import { DashboardComponent }       from './dashboard/dashboard.component';
import { StackComponent }           from './stack/stack.component';
import { ListCheckComponent }       from './stack/list-check/list-check.component';

import { EditFieldComponent }       from './components/edit-field/edit-field.component';
import { EditStackComponent }       from './stack/edit-stack/edit-stack.component';
import { CreateStackComponent }     from './stack/create-stack/create-stack.component';

import { RequestOptions }           from '@angular/http';
import { DefaultRequestOptions }    from './services/custom.options';
import { CheckStackComponent }      from './stack/check-stack/check-stack.component';
import { StatsComponent }           from './stats/stats.component';
import { CheckDatePickerComponent } from './components/check-date-picker/check-date-picker.component';
import { ProfileComponent }         from './profile/profile.component';
import { AboutComponent }           from './about/about.component';
import { YopOptionListComponent }   from './components/yop-option-list/yop-option-list.component';
import { ContextualFooterComponent } from './contextual-footer/contextual-footer.component';
import { NameMatchPipe } from './pipes/name-match.pipe';
import { CreateAccountComponent } from './create-account/create-account.component';
import { PreferenceStackComponent } from './stack/preference-stack/preference-stack.component';
import { KanbanComponent } from './kanban/kanban.component';
import { StartswithPipe } from './pipes/startswith.pipe';
import { UserPickerComponent } from './user/user-picker/user-picker.component';
import { AvatarComponent } from './avatar/avatar.component';


const appRoutes: Routes = [
  { path: 'login',                  component: LoginComponent },
  { path: 'user' ,                  component: UserComponent },
  { path: 'logout',                 component: LogoutComponent},
  { path: 'dashboard',              component: DashboardComponent},
  { path: 'users/:uid/stacks/:sid', component: StackComponent},
  { path: 'profile',           component: ProfileComponent},
  { path: 'about',                  component: AboutComponent},
  { path: 'create-account',         component: CreateAccountComponent},
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'}
];



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    UserComponent,
    LogoutComponent,
    DashboardComponent,
    StackComponent,
    ListCheckComponent,
    EditFieldComponent,
    EditStackComponent,
    CreateStackComponent,
    CheckStackComponent,
    StatsComponent,
    CheckDatePickerComponent,
    ProfileComponent,
    AboutComponent,
    YopOptionListComponent,
    ContextualFooterComponent,
    NameMatchPipe,
    CreateAccountComponent,
    PreferenceStackComponent,
    KanbanComponent,
    StartswithPipe,
    UserPickerComponent,
    AvatarComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    DragDropModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserModule,
    JsonpModule,    
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CookieModule.forRoot()
  ],
  providers: [UserService, 
              StackService, 
              CheckService, 
              CommentService,
              PreferenceService,
              CommonService,
              StackUiLogicService, 
              GlobalService, 
              AuthService,
              ContextDataService,
              BackendInfoService,
              {provide: RequestOptions, useClass: DefaultRequestOptions} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
