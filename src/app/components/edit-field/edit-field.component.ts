import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-field',
  templateUrl: './edit-field.component.html',
  styleUrls: ['./edit-field.component.css']
  })
export class EditFieldComponent implements OnInit {

  @Input() editableText: string;
  @Input() formattedText: string;
  @Input() isEditable: boolean;
  @Input() editLabel: string;
  @Input() okLabel: string;
  @Input() cancelLabel: string;

  @Output() onEditionCompleted: EventEmitter<any> = new EventEmitter();
  @Output() onEditionStarted:   EventEmitter<any> = new EventEmitter();
  @Output() onEditionCanceled:  EventEmitter<any> = new EventEmitter();


  edition: boolean      = false;
  edit_visible: boolean = false;
  initial_text: string  = "";
  optionsObj : any      = {};
  public textarea_row_nb: number = 1;


  constructor() { }

  ngOnInit() {
    if (this.formattedText === undefined || this.formattedText === "") {
      this.formattedText = this.editableText;
    }

    if (this.editLabel == undefined) {
      this.editLabel = "edit";
    }
    if (this.okLabel == undefined) {
      this.okLabel = "ok";
    }
    if (this.cancelLabel == undefined) {
      this.cancelLabel = "cancel";
    }
  }

  public toggle() {
    this.edition = !this.edition;
    // edition end
    if (!this.edition){
      this.editionCompleted();
      this.edit_visible = false;
    } 
    // edition start
    else {
      this.editionStarted();
      this.initial_text = this.editableText;
      // adapting line count of the textarea to the content
      this.textarea_row_nb = Math.min(6,this.editableText.split("\n").length);
    }
  }

  public cancel() {
    this.edition = false;
    this.editableText = this.initial_text;
    this.editionCanceled();
  }


  public editionCompleted(): void {
      this.onEditionCompleted.emit([this.editableText]);
  }

  public editionStarted(): void {
      this.onEditionStarted.emit();
  }

  public editionCanceled(): void {
      this.onEditionCanceled.emit();
  }

  public mouseover_editable() {
    this.edit_visible = true;
  }
}

