import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-yop-option-list',
  templateUrl: './yop-option-list.component.html',
  styleUrls: ['./yop-option-list.component.css']
})
export class YopOptionListComponent implements OnInit {

  @Input()
  public optionlist: string[];
  @Input()
  public selected_value: string;
  public show_list: boolean = false;

  @Output() 
  onOptionSelected: EventEmitter<any> = new EventEmitter();


  constructor() { }

  ngOnInit() {
    console.log("yop-option-list: option-list: ", this.optionlist); 
  }


  public show_option_list() {
    this.show_list = true;

  }

  public option_selected(option: string) {
    this.selected_value = option;
    this.show_list = false;
    this.onOptionSelected.emit([option]);
  }

}
