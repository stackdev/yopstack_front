import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YopOptionListComponent } from './yop-option-list.component';

describe('YopOptionListComponent', () => {
  let component: YopOptionListComponent;
  let fixture: ComponentFixture<YopOptionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YopOptionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YopOptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
