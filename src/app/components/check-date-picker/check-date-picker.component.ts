import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-check-date-picker',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './check-date-picker.component.html',
  styleUrls: ['./check-date-picker.component.css']
})
export class CheckDatePickerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
