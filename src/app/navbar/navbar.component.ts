import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../services/global.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  global: GlobalService;
  location: Location;

  constructor(location: Location, global: GlobalService) {
    this.location = location;
    this.global = global;
  }

  ngOnInit() {
  }

  public isActive(path: string) {
    return this.location.path() === path;
  }

}
