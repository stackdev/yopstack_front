import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';


import { NavbarComponent } from './navbar.component';

import { GlobalService } from '../services/global.service';


describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let mockLocation;

  beforeEach(async(() => {
    mockLocation = jasmine.createSpyObj('Location', ['path']);
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      providers: [
        GlobalService,
        { provide: Location, useValue: mockLocation }
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
