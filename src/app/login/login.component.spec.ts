import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { NO_ERRORS_SCHEMA } from '@angular/core';

import { LoginComponent } from './login.component';
import { NavbarComponent } from '../navbar/navbar.component';

import { GlobalService } from '../services/global.service';
import { UserService } from '../services/user.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  
  let mockRouter;
  let mockUserService;

  beforeEach(async(() => {
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);


    mockUserService = jasmine.createSpyObj('UserService', ['doLogin']);
    mockUserService.doLogin.and.callFake((user, password) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});

    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: UserService, useValue: mockUserService },
        GlobalService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
