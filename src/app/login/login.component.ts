import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';


import { UserService } from '../services/user.service';
import { Credentials } from '../services/credentials';
import { GlobalService } from '../services/global.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  private userService: UserService;
  private global: GlobalService;
  private router: Router;
  public loginError: boolean = false;
  public loginMessage: boolean = false;
  public message: string = "";

  // public for component template access
  public credentials: Credentials;

  constructor(router: Router, public activatedRoute: ActivatedRoute, 
              userService: UserService, global: GlobalService) {

    this.router = router;
    this.userService = userService;
    this.global = global;
    this.credentials = {username:"", password:""};
  }

  ngOnInit() {
    this.activatedRoute.queryParams
    .subscribe(queryParams => {
      console.log("URL parameters: ", queryParams, ", route: ", this.activatedRoute);
      if (queryParams['username']) {
        this.credentials.username = queryParams['username'];
      }

      if (queryParams['logout']) {
        this.loginMessage = true;
        this.message = "Sucessfully logout."
      }

      if (queryParams['newaccount']) {
        this.loginMessage = true;
        this.message = "Hey" + this.credentials.username + ", welcome on yopstack !"
      }
    })
  }

  public login() {
    this.resetErrorAndMessages();
    //TODO: to be moved to the auth service
    this.userService.doLogin(this.credentials.username, this.credentials.password)
    .subscribe(
      user => {  this.global.authenticatedUser = user;
                 this.global.authenticated         = true;
                 console.log("User logged in: ", user);
                 this.router.navigate(['/']);
                 }, 
      e     => { console.error(e);
                this.global.authenticatedUser = undefined;
                this.global.authenticated     = false;
                this.loginError = true;
                this.resetPassword();
              }
      );
  }

  private resetErrorAndMessages() {
    this.loginError = false;
    this.loginMessage = false;
  }

  private resetPassword() {
    this.credentials['password'] = "";
  }


}
