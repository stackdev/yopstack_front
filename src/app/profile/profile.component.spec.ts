import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ProfileComponent } from './profile.component';

import { GlobalService } from '../services/global.service';
import { UserService } from '../services/user.service';


describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  let mockUserService;

  beforeEach(async(() => {

    mockUserService = jasmine.createSpyObj("UserService", ["updateAUser"])
    mockUserService.updateAUser.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});
    TestBed.configureTestingModule({
      declarations: [ ProfileComponent],
      providers: [
        GlobalService,
        { provide: UserService, useValue: mockUserService }
      ],
      schemas: [NO_ERRORS_SCHEMA]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
