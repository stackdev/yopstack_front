import { Component, Input, OnInit } from '@angular/core';
import { GlobalService }     from '../services/global.service';
import { UserService }       from '../services/user.service';
import { User }              from '../services/user';
import { Avatar }            from '../services/avatar';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public globalService: GlobalService;
  private userService: UserService;

  public old_password: string;
  public new_password: string;
  public confirm_new_password: string;
  public update_password_error: boolean = false;
  public password_mismatch_error: boolean = false;

  public timezone_update_error: boolean = false;

  public update_status_success: boolean = false;
  public update_status_text: string = "";
  
  public newAvatarColor: string = "";

  public currentUser: User;
  

  constructor(globalService: GlobalService, userService: UserService) { 
    this.globalService = globalService;
    this.userService = userService;
  }

  ngOnInit() {
    if(this.globalService.authenticatedUser.avatar !== undefined && this.globalService.authenticatedUser.avatar.data !== undefined) {
        this.newAvatarColor = this.globalService.authenticatedUser.avatar.data;
    } else {
      console.log("authenticatedUser in profile: ", this.globalService.authenticated)
    }
  }

  onPasswordFormChange(event: any) {
    this.update_password_error = false;
    this.password_mismatch_error = false;
    this.update_status_text = "";
    if (this.new_password !== this.confirm_new_password ) {
      this.password_mismatch_error = true;
    }
  } 

  update_password() {
    
    if (this.password_mismatch_error || this.new_password == "" ) {
      console.log("Not Resetting pasword.");
      return;
    } 

    console.log("Resetting password !");

    let user_modified: User = this.globalService.authenticatedUser;
    user_modified.setpw = this.new_password;
    user_modified.oldpw = this.old_password;
    console.log(user_modified);
    this.update_status_text = "";

    this.userService
      .updateAUser(user_modified)
      .subscribe(
        user => {
          console.log("User updated ok");
          this.update_password_error = false;
          this.old_password = "";
          this.new_password = "";
          this.confirm_new_password = "";

          this.update_status_text = "Password updated successfully !";
          this.update_status_success = true;
          },
        () => {
          this.update_password_error = true;
          this.update_status_text = "Password update failed !";
          this.update_status_success = false;
        });
  }

  public updateAvatarColor() {
    let authenticatedUser: User;
    let authenticatedUserAvatar: Avatar;
    if (this.globalService.authenticatedUser !== undefined)  {
      authenticatedUser = this.globalService.authenticatedUser;
      authenticatedUserAvatar = this.globalService.authenticatedUser.avatar;
    }
    let newAvatar: Avatar = new Avatar("1", 0, this.newAvatarColor);

    console.log(`new color for avatar picked up: ${this.newAvatarColor}`)
    if(this.newAvatarColor !== undefined && this.newAvatarColor !== "" &&
      newAvatar !== authenticatedUserAvatar) {

        let userModified: User = this.globalService.authenticatedUser;
        userModified.avatar = newAvatar;
        console.log(userModified);
        this.update_status_text = "";
    
        this.userService
          .updateAUser(userModified)
          .subscribe(
            user => {
              console.log("User updated ok");
              this.update_password_error = false;
    
              this.update_status_text = "Avatar updated successfully !";
              this.update_status_success = true;
              },
            () => {
              this.update_status_text = "Avatar update failed !";
              this.update_status_success = false;
            });
      }

  }
}
