import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';


import { AuthService } from '../services/auth.service';

import { GlobalService } from '../services/global.service';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  private router: Router;

  private authService: AuthService;
  private global: GlobalService;


  constructor(authService: AuthService, global: GlobalService, router: Router) { 
    this.authService = authService;
    this.global = global;
    this.router = router;
  }

  ngOnInit() {
    console.log("initiating logout!");
    this.authService.logout()
    .subscribe(
      value => {
        this.endLogoutCallback();
        console.log("logout ok");
      }, 
      e => {
        this.endLogoutCallback();
        console.log("logout failure: ", e );
      });
  }

  public endLogoutCallback = function () {
      console.log("End of logout ?");
      this.authService.authCleanup();
      this.router.navigate(['/login'], {queryParams: {logout: '1'}});
    };

} // end 