import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { LogoutComponent } from './logout.component';

import { GlobalService } from '../services/global.service';
import { AuthService } from '../services/auth.service';

import { Observable } from 'rxjs/Observable';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let mockAuthService;
  let mockRouter;

  beforeEach(async(() => {
    mockAuthService = jasmine.createSpyObj('AuthService', ['loggedIn', 'logout']);
    mockAuthService.loggedIn.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});
    mockAuthService.logout.and.callFake( (user) => { return Observable.create(observer => { observer.next({"name":""});observer.complete(); });});

    mockRouter = jasmine.createSpyObj("Router", ["navigate"]);

    TestBed.configureTestingModule({
      declarations: [ LogoutComponent ],
      providers: [
        GlobalService,
        { provide: AuthService, useValue: mockAuthService },
        { provide: Router, useValue: mockRouter },
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
