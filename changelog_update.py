#!/usr/bin/python 
import json 
import yaml 

yaml_source = "changelog.yaml"
json_changelog = "changelog.json"

def _get_current_version():
    """
    get current version from package.json
    """
    with open('package.json', 'r') as package:
        data = json.load(package)
        # print(data)
        if "version" in data.keys():
            return data["version"]
    return "no_version_found"

def _generate_json():
    with open(yaml_source, 'r') as source:
        data = yaml.load(source)
        with open(json_changelog, 'w') as dest:
            json.dump(data, dest)

if __name__ == "__main__":
    current_version = _get_current_version()
    print("Current version: {}".format(current_version))

    _generate_json()
    # add new version log based on git commit logs

