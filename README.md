# StackTest

- Running jasmine/karma tests: 
```
ng test --sourcemaps=false
```
(sourcemaps=false is adding details when a test is failing)

- Run only one file: change `decribe` to `fdescribe`

# Prod build
ng build --env=prod --output-hashing none --aot

# generate new release
python3 release.py
- dependency on ruamel.yaml (but honestly it is not better than pyaml in py3...)


# deprecated --- increment version
 ~/devel/01_yopstack/yopstack_front[master]$ ../../node_js/node-v6.10.2-linux-x64/bin/npm version patch


### Some TODOS

#### new stuff front and back
- [x] one command backup from prod or dev
- [x] one command restore prod or dev
- [ ] D3js streak view in stat tab (instead of comment size over time...)
- [ ] make creation wizard nicer
- [ ] stack template (- add end date - special user to identify templates)
- [ ] hide date in notes stack per default (new preference needed)
- [ ] allow check resorting by prio (up down button ?)
- [x] preference: enable show period in check
- [x] preference: enable missed period deactivation
- [x] implement get more comments link in stack.component (use to be in list-check.component)
- [ ] really public stack + url shortener ? 
- [ ] full text search with `text` index (https://code.tutsplus.com/tutorials/full-text-search-in-mongodb--cms-24835)


#### known bugs
- [ ] v0.3.6: on new check the frontend only check 'Current period can be checked here' dissapears, probably related to check.formatted not being computed.
- [x] backend fixed in v0.3.6: comment retrieval is limited to 10 also when retrieving it for multiple checks at once.

- [x] v0.1.0:  No missing checks count displayed between current date (without check) and latest check.

- [x] 'future check can be planned here' displayed as failed
- [x] undefined period on check update and creation

- [x] markdown formatting on comment addition or update
- [x] css for bottom button (difficult to click, offset on the left...)
- [x] missing 'missed checks' between 'period 0' check and previous check. (not reproduced...)
