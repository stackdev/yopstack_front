import { StackTestPage } from './app.po';

describe('stack-test App', () => {
  let page: StackTestPage;

  beforeEach(() => {
    page = new StackTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
