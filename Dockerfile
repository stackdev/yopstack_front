FROM nginx:1.14.2-alpine

COPY nginx_default.conf /etc/nginx/conf.d/default.conf
COPY dist/yopstack_front /usr/share/nginx/html
