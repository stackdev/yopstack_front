#!/bin/bash

if [ -z $1 ];
then
  echo "missing version, exiting"
  exit
fi
VERSION="$1"
script_dir="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
echo "script_dir: $script_dir"
cd $script_dir

echo "Building yopstack_front docker image version $VERSION"

cmd="ng build --prod --aot"
echo "Building the frontend prod with:\n$cmd"
$cmd

if [ $? != 0 ];
then
    echo "build failed. Exiting"
    exit 1
fi

echo "cleaning zip and xcf in dist/assets/resources/"
rm $script_dir/dist/yopstack_front/assets/resources/*.zip
rm $script_dir/dist/yopstack_front/assets/resources/*.xcf


echo "Building the docker image"
#TODO rename the repo yopstack_front
docker build . -t yopstack_static:${VERSION}
docker tag yopstack_static:${VERSION} dzd2009/yopstack_static:${VERSION}
echo "Pushing docker image"
docker push dzd2009/yopstack_static:${VERSION} 
